-- 10-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local M = require "clieos"
local U = M.UTILS
-- local Report = M.Report

---@diagnostic disable-next-line: unused-local
local inspect = U.inspect
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format



describe("clieos Report", function()
  it("update", function()
    assert.same(1, 1)
  end)
  --[[


  --
  it("update", function()
    local obj = {
      key = 1
    }
    assert.same(1, obj.key)
    -- to edit object based parsed human input
    local patch = {
      key = { v = 8, t = 'number' }
    }
    local report = Report:new({ patch = patch })
    assert.same({ patch = patch, cnt = 0 }, report)

    local exp = { cnt = 0, patch = { key = { t = 'number', v = 8 } } }
    assert.same(exp, report)

    assert.same({ true, 8 }, { report:update(obj, 'key') })
    assert.same(8, obj.key)
    assert.same(1, report.cnt)

    assert.same(false, report:update(obj, 'key')) -- alredy same value
    assert.same(8, obj.key)
    local exp2 = {
      cnt = 1, -- not changed
      patch = patch,
      updated = { key = true }
    }
    assert.same(exp2, report)
    assert.same(true, report:has_changes())
  end)


  it("update same value - no changes", function()
    local obj = { key = 1 }
    local patch = { key = { v = 1, t = 'number' } }

    local report = Report:new({ patch = patch })

    assert.same({ false, 1 }, { report:update(obj, 'key') })
    assert.same(1, obj.key)
    assert.same(0, report.cnt)
    assert.same(false, report:has_changes())
  end)

  it("update attempt query not_existed key", function()
    local obj = { key = 1 }
    local patch = { key = { v = 8, t = 'number' } }

    local report = Report:new({ patch = patch })

    assert.same({ false, nil }, { report:update(obj, 'not_existed_key') })
    assert.same(1, obj.key)
    assert.same(0, report.cnt)

    local exp = {
      cnt = 0,
      patch = patch,
      not_found = { 'not_existed_key' },
    }
    assert.same(exp, report)
  end)


  it("value query new_value without direct object update", function()
    local obj = { key = 1 }

    local patch = {
      key = { v = 8, t = 'number' }
    }
    local report = Report:new({ patch = patch })

    -- just return new value from patch for given key or current value if
    -- key not found
    assert.same(8, report:value('key', 1, 'number')) -- updated
    assert.same(1, obj.key)

    local exp = { patch = patch, cnt = 0 }
    assert.same(exp, report)
    -- by design, the caller himself should assign a new value to the object field
    -- but this does not happen here, so the state of the object does not change
    assert.same(1, obj.key)
  end)

  it("value + mark manyaly", function()
    local obj = { key = 1 }
    local patch = { key = { v = 8, t = 'number' } }

    local report = Report:new({ patch = patch })

    local new_value = report:value('key', 1, 'number')
    assert.same(0, report.cnt)
    assert.same(false, report:is_updated('key'))

    assert.same(8, new_value)
    assert.same(1, obj.key)

    obj.key = new_value                     -- do the job yourself

    report:set_updated('key', 1, new_value) -- mark manyaly
    assert.same(1, report.cnt)
    assert.same(true, report:is_updated('key'))
  end)

  it("value + rule: trusted_caller", function()
    local obj = { key = 1 }
    local patch = { key = { v = 8, t = 'number' } }

    local report = Report:new({ patch = patch })
    --
    -- Here we say to trust the calling party that it undertakes to receive
    -- a new value and assign it where necessary
    report:get_rules().trusted_caller = true
    local new_value = report:value('key', 1, 'number')
    assert.same(8, new_value)
    assert.same(1, obj.key)
    obj.key = new_value -- do the job yourself

    local exp_repo_state = {
      cnt = 1,
      patch = patch,
      updated = { key = true },
      rules = { trusted_caller = true }
    }
    assert.same(exp_repo_state, report)
  end)

  -- it("update can set nil", function() end)
]]
end)
