-- 10-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("deprecated");

local mk_snapshot0 = M.TESTING.mk_snapshot0

describe("deprecated", function()
  it("mk_snapshot via serialize method", function()
    local obj = {
      a = { x1 = 1, x2 = 2, y1 = 3, y2 = 4 },
      sortedKeys = function()
        return { 'x1', 'y1', 'x2', 'y2' }
      end,
      serialize = function(self)
        return {
          x1 = self.a.x1,
          y1 = self.a.y1,
          x2 = self.a.x2,
          y2 = self.a.y2
        }
      end,
    }
    local exp = {
      { k = 'x1', v = '1', t = 'number' },
      { k = 'y1', v = '3', t = 'number' },
      { k = 'x2', v = '2', t = 'number' },
      { k = 'y2', v = '4', t = 'number' }
    }
    assert.same(exp, M.mk_snapshot(obj, obj.sortedKeys()))
  end)

  it("mk_snapshot direct", function()
    local obj = {
      x1 = 1,
      x2 = 2,
      y1 = 3,
      y2 = 4,
      sortedKeys = function()
        return { 'x1', 'y1', 'x2', 'y2' }
      end,
    }
    local exp = {
      { k = 'x1', v = '1', t = 'number' },
      { k = 'y1', v = '3', t = 'number' },
      { k = 'x2', v = '2', t = 'number' },
      { k = 'y2', v = '4', t = 'number' }
    }
    assert.same(exp, M.mk_snapshot(obj, obj.sortedKeys()))
  end)


  it("build_prompt_oneliner", function()
    local snapshot = {
      { k = 'x1', v = '1', t = 'number' },
      { k = 'y1', v = '2', t = 'number' },
      { k = 'x2', v = '3', t = 'number' },
      { k = 'y2', v = '4', t = 'number' }
    }
    assert.same({ 'x1 y1 x2 y2', '1 2 3 4' }, { M.build_prompt_oneliner(snapshot) })
  end)


  -- ================================================================================

  local function parse_input(p, i, s)
    local st, patch = M.parse_input(p, i, s)
    return tostring(st) .. '|' .. inspect(patch, { newline = '', indent = '' })
  end


  it("parse_input", function()
    local f, mksn = parse_input, mk_snapshot0

    local prompt, input = 'x y color', '1 2 X'
    local sn = { -- snapshot
      { k = 'x',     v = '1', t = 'number' },
      { k = 'y',     v = '2', t = 'number' },
      { k = 'color', v = 'X', t = 'string' },
    }
    local exp = 'true|{{k = "x", v = 1}, {k = "y", v = 2}, {k = "color", v = "X"}}'
    assert.same(exp, f(prompt, input, sn))
    assert.same(sn, mksn({ 'x', 1 }, { 'y', 2 }, { 'color', 'X' }))

    prompt, input, sn = 'x x color', '1 2 Q', mksn({ 'x', 1 }, { 'color', 'Q' })
    assert.match_error(function()
      f(prompt, input, sn)
    end, 'Attempt to override existed Key: \'x\' at: 2')

    prompt, input, sn = ' ', '1', {}
    assert.same('false|"too many values: 1 expected: 0"', f(prompt, input, sn))

    prompt, input, sn = 'a b', '1 2 3', mksn({ 'a', 1 }, { 'b', 2 }, { 'c', 3 })
    assert.same('false|"too many values: 3 expected: 2"', f(prompt, input, sn))

    prompt, input, sn = 'a b', '1', mksn({ 'a', 1 }, { 'b', 2 })
    assert.same('false|"expected at least: 2 values got: 1"', f(prompt, input, sn))

    prompt, input, sn = 'k1 key2', 'v1 v2', mksn({ 'k1', 'v1' }, { 'key2', 'v2' })
    local exp2 = 'true|{{k = "k1", v = "v1"}, {k = "key2", v = "v2"}}'
    assert.same(exp2, f(prompt, input, sn))
  end)


  it("parse_input tonumber", function()
    local f, mksn = parse_input, mk_snapshot0
    local prompt, input = 'x y color', '1 2 X'
    local sn = mksn({ 'x', 1 }, { 'y', 2 }, { 'color', 'X' })
    local exp = 'true|{{k = "x", v = 1}, {k = "y", v = 2}, {k = "color", v = "X"}}'
    assert.same(exp, f(prompt, input, sn))

    prompt, input = 'x x color', '1 2 Q'
    assert.match_error(function()
      f(prompt, input, sn)
    end, 'Attempt to override existed Key: \'x\' at: 2')

    prompt, input = ' ', '1'
    assert.same('false|"too many values: 1 expected: 0"', f(prompt, input, sn))

    prompt, input, sn = 'a b', '1 2 3', mksn({ 'a', 1 }, { 'b', 2 })
    assert.same('false|"too many values: 3 expected: 2"', f(prompt, input, sn))

    prompt, input = 'a b', '1'
    assert.same('false|"expected at least: 2 values got: 1"', f(prompt, input, sn))

    prompt, input = 'key1 key2', 'v1 v2'
    sn = mksn({ 'key1', 's' }, { 'key2', 's' })
    local exp2 = 'true|{{k = "key1", v = "v1"}, {k = "key2", v = "v2"}}'
    assert.same(exp2, f(prompt, input, sn))
  end)


  it("full workflow from prepare to update object", function()
    local obj = {
      x1 = 11, x2 = 12, y1 = 21, y2 = 22
    }
    -- provided by object itself
    local sorted_keys = { 'x1', 'y1', 'x2', 'y2' }
    local data = { x = 0, y = 0 }

    -- prepare snapshot with object state
    local snapshot = M.mk_snapshot(obj, sorted_keys, data)
    -- convert to string to edit by human
    local msg, line = M.build_prompt_oneliner(snapshot)

    assert.same({ 'x1 y1 x2 y2', '11 21 12 22' }, { msg, line })

    -- emulate human editing as plain text
    local input = '31 41 32 42'

    -- create patch based on input
    local ok, patch = M.parse_input(msg, input, snapshot)
    assert.same(true, ok) ---@cast patch table
    local exp = {
      { k = 'x1', v = 31 },
      { k = 'y1', v = 41 },
      { k = 'x2', v = 32 },
      { k = 'y2', v = 42 }
    }
    assert.same(exp, patch)

    local report = {}
    local exp_report = {
      updated = { x2 = true, y2 = true, x1 = true, y1 = true }, cnt = 4
    }
    -- update object fields (table keys)
    assert.same(exp_report, M.update_obj(obj, patch, report))

    assert.same({ x2 = 32, y2 = 42, x1 = 31, y1 = 41 }, obj)
  end)

  it("Report updated_keys_count & isUpdated", function()
    local is_key_updated = M.Report.is_key_updated
    local is_updated = M.Report.is_updated
    local updated = M.Report.updated_keys_count
    local update_v = M.update_value
    local obj = { ka = 1, kb = 4, kc = 'old' }
    local report = {}

    assert.same(false, update_v(obj, 'ka', 1, report))
    assert.same(true, update_v(obj, 'kb', 8, report))
    assert.same(true, update_v(obj, 'kc', 'NEW', report))

    assert.same(2, updated(report, { 'ka', 'kb', 'kc' }))
    assert.same(0, updated(report, { 'ka' }))
    assert.same(1, updated(report, { 'ka', 'kb' }))

    assert.is_false(is_key_updated(report, 'ka'))
    assert.is_false(is_updated(report, 'ka'))
    assert.is_true(is_updated(report, 'kb'))
    assert.is_true(is_updated(report, 'kc'))

    assert.is_false(is_updated(report, 'non-existed'))
    assert.is_true(is_updated(report, 'non-existed', 'kb'))
  end)
end)

describe("clieos testing helpers", function()
  it("tooling mkSnapshot", function()
    local f = mk_snapshot0
    local exp = {
      { k = 'x', v = '4',  t = 'number' },
      { k = 'y', v = '42', t = 'number' },
      { k = 'c', v = 'X',  t = 'string' }
    }
    assert.same(exp, f({ 'x', 4 }, { 'y', 42 }, { 'c', 'X' }))
  end)

  it("mk_patch", function()
    local f = M.TESTING.mk_patch
    local exp = {
      { k = 'color', v = '@' },
      { k = 'x',     v = 3 },
      { k = 'y',     v = 4 }
    }
    assert.same(exp, f({ x = 3, y = 4, color = '@' }))
  end)
end)

describe("clieos testing helpers", function()
  -- return new or old value and track if value changed
  it("update_from", function()
    local f = M.update_from
    local report = {}
    local patch = {
      { k = 'x1', v = 31 },
      { k = 'y1', v = 41 },
    }
    assert.same({ 31, false }, { f(patch, 'x1', 31, report) })
    assert.same({ cnt = 0 }, report)

    assert.same({ 31, true }, { f(patch, 'x1', 88, report) })
    assert.same({ cnt = 1, updated = { x1 = true } }, report)
  end)

  -- by default, if the flag that the value can be set to nil is
  -- not explicitly specified, then do not do it
  it("update_from", function()
    local f = M.update_from
    local report = {}
    local patch = {
      { k = 'x1', v = nil },
      { k = 'y1', v = 41 },
    }
    assert.same({ 88, false }, { f(patch, 'x1', 88, report) })
    local exp = { cnt = 0, nils = { 'x1' } }
    assert.same(exp, report)
  end)

  it("update_from can set nil", function()
    local f = M.update_from
    local report = { can_set_nil = true }
    local patch = {
      { k = 'x1', v = nil },
      { k = 'y1', v = 41 },
    }
    assert.same({ [1] = nil, [2] = true }, { f(patch, 'x1', 88, report) })
    local exp = { updated = { x1 = true }, can_set_nil = true, cnt = 1 }
    assert.same(exp, report)
  end)

  it("value_with_type", function()
    local f = M.value_with_type
    local snapshot = {
      { k = 'x', v = '4',     t = 'number' },
      { k = 'y', v = '42',    t = 'number' },
      { k = 'c', v = 'X',     t = 'string' },
      { k = 'd', v = 'value', t = 'string' },
    }
    assert.same(8, f('x', '8', snapshot))
    assert.same('X', f('c', 'X', snapshot))
    assert.same(nil, f('y', 'nil', snapshot))
    assert.same(nil, f('d', 'nil', snapshot))
  end)
end)
