---@diagnostic disable: undefined-global
-- 11-05-2024 @author Swarg
local M = {}

---@class clieos.Report
---@field new fun(self, o:table?): clieos.Report
---@field not_found table?
---@field nils table?
---@field updated table?
---@field patch table?
---@field rules table?
local Report = {
  _cname = 'clieos.Report'
}

M.Report = Report
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

--

-- The main purpose of the report is to store information about which specific
-- fields were changed and which were not found, while simultaneously checking
-- the synchronization of the patch and the constructed snapshot of the state
-- of the object at the time of the start of editing it.

---@return clieos.Report
function Report:new(o)
  if not type(o) == 'table' then
    error(fmt('expected table report or nil got: %s', o))
  end
  o = o or {}
  o.cnt = o.cnt or 0

  setmetatable(o, self)
  self.__index = self

  return o
end

function Report:get_rules()
  self.rules = self.rules or {}
  return self.rules
end

---@param key string
---@param old_value any
---@param vtype string?  -- to sync with snapshot and patch
---@diagnostic disable-next-line: unused-local
function Report:value(key, old_value, vtype)
  assert(type(key) == 'string', 'key')
  local patch = assert_tbl_ne(self.patch, 'patch')
  local wrapped = patch[key]
  if not wrapped then
    self:set_not_found(key)
    return old_value
  end

  if vtype and wrapped.t ~= vtype then
    error(U.format('desynchronized type:%s in patch:%s', vtype, wrapped.t))
  end
  local can_set_nil = (self.rules or E).can_set_nil

  local trust = (self.rules or E).trusted_caller
  local new_value = wrapped.v

  if new_value ~= old_value then
    if new_value == nil then
      if can_set_nil then
        return old_value
      end
      if trust then self:set_nil_value(key) end
    end

    if trust then self:set_updated(key, old_value, new_value) end
    -- updating the object field itself is the responsibility of the caller
    return new_value
  end

  return old_value
end

--
-- update object field with given name(key) based on patch with new value
--
---@param obj table
---@param key string
function Report:update(obj, key)
  assert(type(key) == 'string', 'key')
  assert_tbl(obj, 'obj')
  local patch = assert_tbl_ne(self.patch, 'patch')

  local can_set_nil = (self.rules or E).can_set_nil
  local wrapped = patch[key]
  if not wrapped then
    self:set_not_found(key)
    return false, nil
  end

  local new_value = wrapped.v
  local old_value = obj[key]

  if old_value ~= new_value then
    if new_value == nil then
      if not can_set_nil then
        return false, old_value
      end

      self:set_nil_value(key)
    end

    obj[key] = new_value
    self:set_updated(key, old_value, new_value)
    return true, new_value
  end

  return false, old_value
end

--
-- mark given key as not found
--
-- track keys for which there were requests and which were not in the patch
function Report:set_not_found(key)
  if self and key then
    self.not_found = self.not_found or {}
    table.insert(self.not_found, key)
  end
end

--
-- to track assignment of nil values
--
function Report:set_nil_value(key)
  if self and key then
    log_trace('set nil value key:%s old:%s', key, BT)
    self.nils = self.nils or {}
    table.insert(self.nils, key)
  end
end

--
-- to track changes when editing via the command line
-- mark the key as having a changed value
--
---@param self table?
---@param key string
---@param old_val any
---@param new_val any
function Report:set_updated(key, old_val, new_val)
  if self and key then
    log_trace('updated key %s old:%s new:%s', key, old_val, new_val, BT)
    self.cnt = self.cnt + 1
    self.updated = self.updated or {}
    self.updated[key] = true
  end
end

-- is given key already updated in object
---@param self table?
---@param key string
function Report:is_key_updated(key)
  return ((self or E).updated or E)[key or false] == true
end

--
-- is given key changed by human input
-- powered by patch from Editor
--
---@return boolean?
function Report:is_key_changed(key)
  if self then
    assert_tbl_ne(self.patch, 'patch')
    local wrapped = self.patch[key]
    return (wrapped or E).u
  end
end

-- the number of keys from given keynames varargs(list)
-- whose values marked as "updated" by the user
--
---@param self table?
---@return number
function Report:get_updated_cnt(...)
  local c = 0
  if self and self.updated then
    for i = 1, select('#', ...) do
      local key = select(i, ...)
      if self.updated[key] then
        c = c + 1
      end
    end
  end
  return c
end

--
-- returns true if at least one of the given keys has been updated
-- that is, the value was changed by the user
--
---@return boolean
function Report:is_updated(...)
  return Report.get_updated_cnt(self, ...) > 0
end

---@return number
function Report:get_changed_cnt(...)
  local c = 0
  if self then
    assert_tbl_ne(self.patch, 'patch')

    for i = 1, select('#', ...) do
      local key = select(i, ...)
      local wrapped = self.patch[key]
      if not wrapped then
        self:set_not_found(key)
      end
      if wrapped.u then
        c = c + 1
      end
    end
  end
  return c
end

---@return boolean
function Report:is_changed(...)
  return Report.get_changed_cnt(self, ...) > 0
end

---@param self table?
function Report:has_changes()
  assert(type(self) == 'table', 'report')
  return (self.cnt or 0) > 0
end

return M
