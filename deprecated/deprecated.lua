-- 10-05-2024 @author Swarg
---@diagnostic disable: undefined-global

local M, U, T = {}, {}, {}
M.Editor = C
M.UTILS = U
M.TESTING = T

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local log_debug, is_function

---@param snapshot table{{k,v,t}, {k,v,t}}
---@return string prompt message with keys
---@return string line - current values coresponents to keys in prompts
function M.build_prompt_oneliner(snapshot)
  local msg, def = '', ''
  local wrap_value = U.wrapq_value

  for _, e in ipairs(snapshot) do
    if msg ~= '' then msg = msg .. ' ' end
    msg = msg .. v2s(e.k)

    if def ~= '' then def = def .. ' ' end
    def = def .. wrap_value(e.v)
  end

  return msg, def
end
--
---@param obj table
---@param sorted_keys table ordered list of keys
---@param userdata any to pass into serialize function of the obj
---@return table{}
function M.mk_snapshot(obj, sorted_keys, userdata)
  log_debug('mk_snapshot o:%s keys:%s data:%s', obj, sorted_keys, userdata)
  assert(type(sorted_keys) == 'table' and next(sorted_keys), 'sorted_keys')
  local snapshot = {}

  local serialized = nil

  local serialize_fn = obj[M.METHOD_NAME_SERIALIZE]
  local direct = true
  if is_function(serialize_fn) then
    serialized = serialize_fn(obj, userdata)
    assert(serialized, 'has serialized data from method')
    direct = false
  end

  for _, key in pairs(sorted_keys) do
    local v = nil

    if direct then
      local ok, val = M.call_getter(obj, key)
      if ok then
        v = val
      else
        v = obj[key]
      end
    else
      v = (serialized or E)[key]
    end

    local value, vtype = U.as_string(v)

    snapshot[#snapshot + 1] = {
      k = key,
      v = value,
      t = vtype,
    }
  end

  return snapshot
end

--
-- with sync keys and args count
-- old name cli_parse
--
---@param prompt string
---@param line string
---@return boolean
---@return table|string
function M.parse_input(prompt, line, snapshot)
  assert(type(prompt) == 'string' and prompt ~= '', 'expected string prompt')
  assert(type(line) == 'string', 'line')
  assert(type(snapshot) == 'table', 'snapshot') -- original values + types

  local args = M.cli_parse_line(line)
  local patch, set, kn = {}, {}, 1

  for key in string.gmatch(prompt, '([^%s]+)') do
    if set[key] then
      error(fmt("Attempt to override existed Key: '%s' at: %s", key, kn))
    end
    local value = args[kn]

    patch[#patch + 1] = { k = key, v = M.value_with_type(key, value, snapshot) }
    set[key] = key -- to track unique keys
    kn = kn + 1
  end

  kn = kn - 1 -- to key count

  if #args < kn then
    return false, fmt('expected at least: %s values got: %s', v2s(kn), v2s(#args))
  end
  if #args > kn then
    return false, fmt('too many values: %s expected: %s', #args, kn)
  end

  log_debug('cli_parse parsed:', prompt, line, patch)
  return true, patch
end

function C.value_to_type(key, value, snapshot)
  local vt = M.get_type_of(key, snapshot)
  if not vt then
    error(fmt('Not Found type for key: "%s" snapshot:%s',
      v2s(key), v2s(U.inspect(snapshot))))
  end

  if vt == 'number' then
    return tonumber(value) -- can be nil?
    --
  elseif vt == 'string' then
    if value == 'nil' then return nil end
    if value == '""' then return '' end
    return v2s(value)
    --
  elseif vt == 'table' then
    error('Not implemented yet')
  end
  error('unknown type:' .. v2s(vt) .. ' for key: ' .. v2s(key))
end

--------------------------------------------------------------------------------
--                               Updating
--------------------------------------------------------------------------------

--
-- update all fields in the obj by patch values provided in patch
--
---@param obj table
---@param patch table
---@param report table
function M.update_obj(obj, patch, report)
  report = report or {}
  assert(type(patch) == 'table', 'patch')

  for _, e in ipairs(patch) do
    -- e.v can be nil
    M.update_value(obj, e.k, e.v, report)
  end

  return report
end

--
-- update one value in the given table(object) for given key (field name)
--
-- here the new value goes to the callers.
-- but in general it should be taken from the patch
--
--
---@param obj table
---@param key string  filed in the obj
---@param new_value any
---@param report table{0} -- counter report to track updated keys
function M.update_value(obj, key, new_value, report)
  assert(key, 'not nullable key expected')
  Report.init_cnt(report)

  -- collect nillable new values
  if new_value == nil then
    Report.add_nil(report, key)
    return false
  end

  local old_val = obj[key]
  if old_val ~= new_value and new_value ~= nil then
    obj[key] = new_value
    Report.update_key(report, key, old_val, new_value)
    return true
  end

  return false
end

-- pairs are used so that even if the value is early nil but
-- the key has been specified - provide a way to know about this
--
-- search in preserved key order (map?)
-- e - entry of {k = 'keyname', v = 'somevalue'}   where v can be nil
---@param patch table
---@param key string
function M.find_kv_in_patch(patch, key)
  for _, e in ipairs(patch) do
    -- e - entry of {k = 'keyname', v = 'somevalue'}   where v can be nil
    if e.k == key then
      return e
    end
  end
end

local function update_logic(report, key, old_value, new_value)
  local curr_value, updated = old_value, false

  if not report.can_set_nil then
    if new_value == nil then
      Report.add_nil(report, key)
      return old_value, false
    end
  end

  if old_value ~= new_value then
    Report.update_key(report, key, old_value, new_value)
    updated, curr_value = true, new_value
  end

  return curr_value, updated
end

--
-- if has report.can_set_nil then can unpdate value by nil
-- otherwise, without this flag, the old value does not change to empty
--
-- update old_value (with name key) by value from patch
---@return any, boolean
function M.update_from(patch, key, old_value, report)
  assert(type(patch) == 'table', 'patch')
  Report.init_cnt(report)

  local entry = M.find_kv_in_patch(patch, key)
  if not entry then
    Report.not_found(patch, key)
  else
    local new_value = entry.v

    return update_logic(report, key, old_value, new_value)
  end

  return old_value, false
end

---@param obj table
---@param key string
---@param patch table
---@param report table?
function M.update_key_in_obj(obj, key, patch, report)
  assert(type(patch) == 'table', 'patch')
  Report.init_cnt(report)

  local old_value = obj[key]
  local entry = M.find_kv_in_patch(patch, key)

  if not entry then
    Report.not_found(patch, key)
  else
    local new_value = entry.v

    return update_logic(report, key, old_value, new_value)
  end

  return old_value, false
end

--
-- update the given old_value and return the new value or
-- old value if the value has not changed
--
---@param old_val any
---@param key string
---@param new_val any?
---@param report table?
---@param updater function?
---@param obj table?
function M.update_old_value(old_val, key, new_val, report, updater, obj)
  assert(key, 'not nullable key expected')
  Report.init_cnt(report)

  -- collect nillable new values
  if new_val == nil then
    Report.add_nil(report, key)
    return old_val
  end

  if old_val ~= new_val and new_val ~= nil then
    -- to display an empty string in the console, use two double quotes
    if old_val == '""' and new_val == '' then ----  ????????
      -- so consider that there have been no changes here
      return old_val
    end

    M.update_reportkey(report, key, old_val, new_val)
    if type(updater) == 'function' then
      updater(obj, new_val)
    end

    return new_val
  end

  return old_val
end

---@param snapshot table
---@param key string
function M.get_type_of(key, snapshot)
  for _, e in pairs(snapshot) do
    if e.k == key then
      return e.t
    end
  end
  return nil
end

--
-- convert value from srting representation(text) back to actual type
--
---@param key string
---@param value string
---@param snapshot table
function M.value_with_type(key, value, snapshot)
  local vt = M.get_type_of(key, snapshot)
  if not vt then
    error(fmt('Not Found type for key: "%s" snapshot:%s',
      v2s(key), v2s(U.inspect(snapshot))))
  end

  if vt == 'number' then
    return tonumber(value) -- can be nil?
    --
  elseif vt == 'string' then
    if value == 'nil' then return nil end
    if value == '""' then return '' end
    return v2s(value)
    --
  elseif vt == 'table' then
    error('Not implemented yet')
  end
  error('unknown type:' .. v2s(vt) .. ' for key: ' .. v2s(key))
end

-- local function update_logic(report, key, old_value, new_value)
--   local curr_value, updated = old_value, false
--
--   if not report.can_set_nil then
--     if new_value == nil then
--       Report.add_nil(report, key)
--       return old_value, false
--     end
--   end
--
--   if old_value ~= new_value then
--     Report.update_key(report, key, old_value, new_value)
--     updated, curr_value = true, new_value
--   end
--
--   return curr_value, updated
-- end

-- todo
---@param dst table
---@param src table
---@param name string
---@param n number?
-- function Report.merge_update_report(dst, src, name, n)
function Report.merge_reports(dst, src, name, n)
  assert(type(dst) == 'table', 'dst report')
  assert(type(src) == 'table', 'src report')

  dst.cnt = (dst.cnt or 0) + (src.cnt or 0)
  dst.updated = dst.updated or {}
  dst.updated['sub_' .. v2s(name) .. v2s(n or '')] = src

  return dst
end

--------------------------------------------------------------------------------

--
-- direct
--
function C:update_value(key, new_value)
  assert(key, 'not nullable key expected')
  local obj = self.obj
  local can_set_nil = self.rules.can_set_nil

  local old_val = obj[key]

  if new_value == nil and old_val ~= nil then
    if not can_set_nil then
      return false
    end
    -- collect nillable new values
    self:report_set_nil_value(key)
  end

  if old_val ~= new_value then -- and new_value ~= nil then
    obj[key] = new_value
    self:report_set_updated(key, old_val, new_value)
    return true
  end

  return false
end


--
return M
