---@diagnostic disable: undefined-global

  -- deprecated
  it("cli_parse unchanged input", function()
    local r = Rectangle:new(nil, 1, 1, 8, 8)
    local prompt, _, editor = r:cli_prompt({ cli = true })
    local exp = {
      obj = r,
      orderedkeys = {
        { k = 'x1',      t = 'number' },
        { k = 'y1',      t = 'number' },
        { k = 'x2',      t = 'number' },
        { k = 'y2',      t = 'number' },
        { k = 'hline',   t = 'string' },
        { k = 'vline',   t = 'string' },
        { k = 'corners', t = 'string' },
        { k = 'bg',      t = 'string' },
        { k = 'w',       t = 'number' },
        { k = 'h',       t = 'number' }
      },
      snapshot = {
        x1 = { s = '1', o = 1 },
        y1 = { s = '1', o = 1 },
        x2 = { s = '8', o = 8 },
        y2 = { s = '8', o = 8 },
        h = { s = '8', o = 8 },
        w = { s = '8', o = 8 },
        bg = { s = '', o = '' },
        vline = { s = '|', o = '|' },
        hline = { s = '-', o = '-' },
        corners = { s = '++++', o = '++++' },
      },
      rules = {},
      names = {
        apply_patch = 'edit',
        serialize = 'serialize',
        orderedkeys = 'orderedkeys'
      },
      serialize_opts = { cli = true },
      prompt = 'x1 y1 x2 y2 hline vline corners bg w h',
      def_values = '1 1 8 8 "-" "|" "++++" "" 8 8',
    }
    assert.same(exp, editor)

    local unchanged_input = '1 1 8 8 "-" "|" "++++" "" 8 8'

    local ok, patch = editor:parse_input(prompt, unchanged_input)

    assert.same({ false, 'unchanged' }, { ok, patch })
  end)

  it("edit no changes", function()
    local r = Rectangle:new(nil, 1, 1, 8, 8)
    local cr = r:copy()
    local msg, def, editor = r:cli_prompt({ cli = true })
    assert.same('x1 y1 x2 y2 hline vline corners bg w h', msg)
    assert.same('1 1 8 8 "-" "|" "++++" "" 8 8', def)
    local exp = {
      x1 = { s = '1', o = 1 },
      y1 = { s = '1', o = 1 },
      x2 = { s = '8', o = 8 },
      y2 = { s = '8', o = 8 },
      h = { s = '8', o = 8 },
      w = { s = '8', o = 8 },
      bg = { s = '', o = '' },
      vline = { s = '|', o = '|' },
      hline = { s = '-', o = '-' },
      corners = { s = '++++', o = '++++' },
    }
    assert.same(exp, editor.snapshot)

    assert.same(cr, r) -- before update

    local input_line = def
    local st, patch = editor:parse_input(msg, input_line)
    assert.same({ false, 'unchanged' }, { st, patch })
    assert.is_nil(editor.patch)
    -- local updated, report = r:edit(editor.patch) -- workload

    -- assert.same(false, updated)
    -- assert.same({ cnt = 0, can_set_nil = false }, report)
    -- assert.same(cr, r)
  end)

