install:
	sudo luarocks make

VERSION := 0.6.1-1

upload:
	luarocks upload rockspecs/clieos-$(VERSION).rockspec
