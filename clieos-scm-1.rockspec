---@diagnostic disable: lowercase-global
package = "clieos"
version = "scm-1"

source = {
  url = "git+https://gitlab.com/lua_rocks/clieos"
}
description = {
  summary = "command-line-interface to edit objects state",
  detailed = [[
  Lua module that provides an interface for editing the state of an object
  as text in a human-friendly form.
  Designed for integration into CLI applications.

  Based on a given list of sorted keys, creates a prompt-description and
  serialize values of given table(object) to plain text to edit by human.
  Then parses this text and applies updates back to the object.
  Simultaneously creating a detailed report of what exactly was updated.

  workflow:
    - request from the object or build itself a sorted list of keys and their types
    - serialize object state to display as plain text
    - remembering the types of each property field of an object (table)
    - formation of a one-liner for human editing
    - parsing human input(text) back into values of appropriate types.
    - applying changes to the object(table)
    - provide a report on the work done

  supported features:
    - detailed report on the update process and queries for key values
    - restricting access to object fields using a predefined set of keys
    - sorted order of keys when generating a message for a human
    - applying changes to object fields in a given order
    - logging
]],
  homepage = "https://gitlab.com/lua_rocks/clieos",
  license = "MIT"
}

dependencies = {
  -- alogger optional
}

build = {
  type = "builtin",
  modules = {
    clieos = "src/clieos.lua"
  }
}
