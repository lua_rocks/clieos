-- 12-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local M = require "clieos"
local U = M.UTILS
local Patch = M.Patch
local UPDATED_KEY = M.UPDATED_KEY

---@diagnostic disable-next-line: unused-local
local inspect = U.inspect
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


describe("clieos.Patch", function()
  it("inject_state to regular table", function()
    local patch = {}
    assert.is_nil(patch.get_state)
    assert.is_nil(patch._cname)

    Patch.inject_state(patch)
    assert.same({}, patch)
    assert.is_function(patch.get_state)
    assert.same({ updated = {} }, patch:get_state())
    assert.is_nil(patch._cname)
  end)

  it("Patch:new", function()
    local patch = Patch:new({})
    assert.same({}, patch)
    assert.same('clieos.Patch', patch._cname)        -- class name
    assert.same({ updated = {} }, patch:get_state()) -- hidden state for report
  end)

  it("Patch:new", function()
    assert.same({}, Patch:new({}))
    assert.same({}, Patch:new())
    assert.same({ key = 'value' }, Patch:new({ key = 'value' }))
  end)

  it("Patch:get_state() :get_rules()", function()
    local opts = {
      rules = { some = true }
    }
    local p = Patch:new({}, opts)

    local exp = { updated = {}, rules = { some = true } }
    assert.same(exp, p:get_state())

    assert.same({ some = true }, p:get_rules())
  end)

  it("Patch:get_state() :get_rules()", function()
    local p = Patch:new()
    assert.same({}, p)
    assert.same({ updated = {} }, p:get_state())

    assert.same({}, p:get_rules()) -- workload
    p:get_rules().can_set_nil = false

    local exp = { updated = {}, rules = { can_set_nil = false } }
    assert.same(exp, p:get_state())
    assert.same({ can_set_nil = false }, p:get_rules())
    assert.same({}, p)
  end)

  it("Patch:set_rule()", function()
    local p = Patch:new()
    assert.same({}, p)

    assert.same({}, p:get_rules()) -- workload
    p:set_rule('can_set_nil', false)

    local exp = { updated = {}, rules = { can_set_nil = false } }
    assert.same(exp, p:get_state())
    assert.same({ can_set_nil = false }, p:get_rules())
    assert.same({}, p)
  end)

  it("Patch:get_data() serialize_data", function()
    local opts = {
      data = { 'something' }
    }
    local p = Patch:new({}, opts)

    local exp = { updated = {}, data = { 'something' } }
    assert.same(exp, p:get_state())

    assert.same({ 'something' }, p:get_data())
  end)

  it("report_set_nil_value", function()
    local patch = Patch:new()
    assert.same({}, patch)
    assert.same({ updated = {} }, patch:get_state())

    Patch.report_set_nil_value(patch, 'key')
    -- note here you can get an inconsistent state if you forget to call the
    -- report_set_updated method in your code after this method
    assert.same({}, patch)
    local exp = { assigned_nils = { key = 1 }, updated = {} }
    assert.same(exp, patch:get_state())
  end)

  it("report_set_not_found", function()
    local patch = Patch:new()
    assert.same({}, patch)
    assert.same({ updated = {} }, patch:get_state())

    Patch.report_set_not_found(patch, 'key')
    assert.same({}, patch)
    assert.same({ updated = {}, not_found = { key = 1 } }, patch:get_state())
  end)


  it("report_set_updated success synchronized", function()
    local patch = Patch:new({
      key = { old = 'old_value', new = 'new_value' },
    })
    local exp_patch = { key = { new = 'new_value', old = 'old_value' } }
    assert.same(exp_patch, patch)
    assert.same({ updated = {} }, patch:get_state())

    Patch.report_set_updated(patch, 'key', 'old_value', 'new_value')
    assert.same(exp_patch, patch)
    assert.same({ updated = { key = UPDATED_KEY } }, patch:get_state())
  end)


  it("report_set_updated fail desynchronized old_value", function()
    local patch = Patch:new({
      key = { old = '123', new = '456' },
    })
    local exp_patch = { key = { old = '123', new = '456', } }
    assert.same(exp_patch, patch)
    assert.same({ updated = {} }, patch:get_state())

    assert.match_error(function()
      Patch.report_set_updated(patch, 'key', 'bad', '456')
    end, 'key: expected oldInPatch equal givenOldVal got: "123" != "bad"')

    assert.same(exp_patch, patch)
    assert.same({ updated = {} }, patch:get_state())
  end)

  it("report_set_updated fail desynchronized new_value", function()
    local patch = Patch:new({
      key = { old = '123', new = '456' },
    })
    local exp_patch = { key = { old = '123', new = '456', } }
    assert.same(exp_patch, patch)
    assert.same({ updated = {} }, patch:get_state())

    assert.match_error(function()
      Patch.report_set_updated(patch, 'key', '123', 'bad')
    end, 'key: expected oldInPatch equal givenOldVal got: "456" != "bad"')

    assert.same(exp_patch, patch)
    assert.same({ updated = {} }, patch:get_state())
  end)


  it("report_set_error", function()
    local patch = Patch:new({
      key = { old = 23, new = 23 },
    })
    assert.same({ updated = {} }, patch:get_state())
    local error0 = { id = M.TYPE_MISMATCH, details = { v = 'ab', et = 'number' } }

    Patch.report_set_error(patch, 'key', error0)

    local exp = {
      errors = { key = error0 },
      updated = {}
    }
    assert.same(exp, patch:get_state())
  end)


  it("is_changed_to", function()
    local patch = Patch:new({
      key = { old = '123', new = '456' },
    })
    local f = Patch.is_changed_to
    assert.same(true, f(patch, 'key', '456'))
    assert.same(false, f(patch, 'key', '123'))
  end)

  it("is_changed_to from(old)", function()
    local patch = Patch:new({
      key = { old = 'old', new = 'new' },
    })
    local f = Patch.is_changed_to
    assert.same({ true, 'old', 'new' }, { f(patch, 'key', 'new') })
    assert.same({ false, 'old', 'new' }, { f(patch, 'key', 'another') })
  end)
end)
