-- 10-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local clieos = require "clieos"
local M = clieos.UTILS

describe("clieos utils asserts", function()
  it("assert_table", function()
    local f = M.assert_table
    local t = {}
    assert.same(t, f(t, 'table expected got: %s', nil))

    assert.match_error(function()
      f(nil, 'table expected got: %s for key: %s', nil, nil)
    end, 'table expected got: nil for key: nil')

    assert.match_error(function()
      f(nil, 'just_table_name')
    end, 'just_table_name: expected table got: .nil. nil')

    assert.match_error(function()
      f(42, 'some_name')
    end, 'some_name: expected table got: .number. 42')
  end)

  it("assert_table_not_empty", function()
    local f = M.assert_table_not_empty
    local err = "expected not empty table got: .table. {}"
    assert.match_error(function() f({}) end, err)

    err = "expected not empty table got: .nil. nil"
    assert.match_error(function() f(nil) end, err)

    err = "expected not empty table got: .number. 0"
    assert.match_error(function() f(0) end, err)

    err = "expected 1 nil"
    assert.match_error(function() f(0, 'expected %s %s', 1, nil) end, err)
  end)

  it("assert_table_not_empty auto agg got-value", function()
    local f = M.assert_table_not_empty
    local err = "key: expected not empty table got: {}"
    assert.match_error(function()
      f({}, '%s: expected not empty table got: %s', 'key')
    end, err)

    local err2 = "key: expected not empty table got: 42"
    assert.match_error(function()
      f(42, '%s: expected not empty table got: %s', 'key')
    end, err2)
  end)

  it("assert_not_nil", function()
    local f = M.assert_not_nil
    assert.same(1, f(1))
    assert.same(0, f(0))
    assert.same(false, f(false))
    assert.same('1', f('1'))

    assert.match_error(function()
      f(nil)
    end, 'expected not nil, got: nil')

    assert.match_error(function()
      f(nil, 'expected not nil for: "%s" in %s', 'some_key', nil)
    end, 'expected not nil for: "some_key" in nil')
  end)

  it("assert_string", function()
    local f = M.assert_string
    assert.same('x', f('x', 'key'))

    local err = 'key: expected srting, got: 42'
    assert.match_error(function() f(42, 'key') end, err)
  end)

  it("get_name_n_fmt", function()
    local f = M.get_fmt_n_key
    assert.same({ [2] = '' }, { f(nil) })

    assert.same({ 'expected one got: %', '' }, { f('expected one got: %') })
    assert.same({ 'not a key', '' }, { f('not a key') })
    assert.same({ '', '' }, { f('') })
    assert.same({ [2] = '0: ' }, { f('0') })

    assert.same({ [2] = 'key: ' }, { f('key') })
    assert.same({ [2] = 'some_id: ' }, { f('some_id') })
    assert.same({ [2] = 'some-id: ' }, { f('some-id') })
    assert.same({ [2] = 'name88: ' }, { f('name88') })
  end)

  it("assert_eq", function()
    local f = M.assert_eq
    assert.same(true, f(1, 1))
    local err = 'expected equal got: 1 & 2'
    assert.match_error(function() f(1, 2) end, err)

    err = 'expected equal a_&_b: got: 1 & 2'
    assert.match_error(function() f(1, 2, 'a_&_b') end, err)

    err = 'expected equal got: 1 & "1"'
    assert.match_error(function() f(1, '1') end, err)
  end)

  it("assert_eq_in", function()
    local f = M.assert_eq_in
    assert.same(true, f(1, 1, 'x', 'y', 'key'))
    local err = 'key: expected x equal y got: 1 != "1"'
    assert.match_error(function() f(1, '1', 'x', 'y', 'key') end, err)

    err = '%?: expected left equal right got: 1 != 2'
    assert.match_error(function() f(1, 2) end, err)

    err = '%?: expected left equal right got: 1 != 2'
    assert.match_error(function() f(1, 2, "", "", "") end, err)

    err = 'context: expected left equal right got: 1 != 2'
    assert.match_error(function() f(1, 2, nil, nil, "context") end, err)
  end)
end)
