-- 10-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local M = require "clieos"
local U = M.UTILS
---@diagnostic disable-next-line: unused-local
local inspect = U.inspect
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

describe("clieos utils parser", function()
  it("cli_parse_line simple", function()
    local f = M.cli_parse_line
    assert.same({}, f(''))
    assert.same({ 'a' }, f('a'))
    assert.same({ 'ab' }, f('ab'))
    assert.same({ 'ab', 'c' }, f('ab c'))
    assert.same({ '1', '2', '3' }, f('1 2 3'))
    assert.same({ '1', '2', '3' }, f('1 2 3 '))
    assert.same({ '1', '2', '3' }, f('1 2 3   '))
    assert.same({ '1', '2', '3' }, f(' 1 2 3  '))
    assert.same({ '1', '2', '3' }, f('  1 2 3  '))
    assert.same({ '1', '2', '3' }, f('  1  2  3  '))
  end)

  it("cli_parse_line quotes", function()
    local f = M.cli_parse_line
    assert.same({ 'a', '', 'b', ' ' }, f('a "" b " "'))
    assert.same({ '' }, f('""'))
    assert.same({ ' ' }, f('" "'))
    assert.same({ ' ', ' b ' }, f('" " " b "'))
    assert.same({ ' ', '', '' }, f('" " """"'))
    assert.same({ '', 'a' }, f('""a'))
    assert.same({ 'a', '' }, f('a""'))
    assert.same({ 'a', '', 'b' }, f('a""b'))
    assert.same({ 'a', 'c', 'b' }, f('a"c"b'))
    assert.same({ 'a', ' c ', 'b' }, f('a" c "b'))
  end)

  it("cli_parse_line escapes", function()
    local f = M.cli_parse_line
    assert.same({ '"' }, f('\\"'))
    assert.same({ '""' }, f('\\"\\"'))
    assert.same({ '""', ' inside ', '' }, f('\\"\\" " inside " ""'))
  end)

  it("match", function()
    local f = function(line) return string.match(line, '^[xy]%d?$') end
    assert.same('x1', f('x1'))
    assert.same('x2', f('x2'))
    assert.same('y2', f('y2'))
    assert.same('x', f('x'))
    assert.same('y', f('y'))
  end)
end)
