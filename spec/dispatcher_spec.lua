-- 12-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local clieos = require "clieos"
local M = clieos
local P = clieos.Patch

-- example of how to plug builtin command handler into clieosEditor
-- idea: allow you to execute certain built-in commands defined in user code
-- directly while editing an object.

describe("clieos", function()
  local function new_obj()
    local obj = {
      sub1 = {
        a = 1,
        b = false,
      },
      sub2 = {
        c = 'text',
        d = nil,
      },

      orderedkeys = function()
        return { { a = 'num' }, { b = 'bool' }, { c = 'str' }, { d = 'str' } }
      end,

      serialize = function(self)
        return {
          a = self.sub1.a, b = self.sub1.b, c = self.sub2.c, d = self.sub2.d
        }
      end,

      -- edit
      [M.METHOD_NAME_APPLY_PATCH] = function(self, patch)
        self.sub1.a = P.update_one_out(patch, 'a', self.sub1.a)
        self.sub1.b = P.update_one_out(patch, 'b', self.sub1.b)
        self.sub2.c = P.update_one_out(patch, 'c', self.sub2.c)
        self.sub2.d = P.update_one_out(patch, 'd', self.sub2.d or M.NIL)

        -- mark as updated vithout validating patch-obj sync
        P.report_set_updated_risky(patch, 'a', 'b', 'c', 'd')

        return P.updated_cnt(patch), patch
      end
    }
    return obj
  end

  it("tooling orderedkeys", function()
    local obj = new_obj()
    local exp = { { a = 'num' }, { b = 'bool' }, { c = 'str' }, { d = 'str' } }
    assert.same(exp, obj:orderedkeys())
  end)

  ----------------------------------------------------------------------

  ----------------------------------------------------------------------

  it("dispatcher no response", function()
    local test_logs = {}

    local external_dispatcher_func = function(ctx, obj, args)
      test_logs[#test_logs + 1] = { ctx, obj, args }
      -- here the dispatcher must returns success_code and message
    end

    local obj = new_obj()

    local opts = {
      context = { id = 1 },
      dispatcher = external_dispatcher_func,
    }

    local editor = M.init(obj, opts)
    local prompt, _ = editor:build_prompt_oneliner()
    local user_input = '1 "-" text nil \\command a1'

    assert.match_error(function()
      editor:parse_input(prompt, user_input)
    end, 'Seems the dispatcher is not returning any response')

    -- ensure external_dispatcher_func was called
    local exp_logs = { { { id = 1 }, obj, { 'command', 'a1' } } }
    assert.same(exp_logs, test_logs)
  end)


  it("dispatcher is function success", function()
    local test_logs = {}

    local external_dispatcher_func = function(ctx, object, args)
      test_logs[#test_logs + 1] = { ctx, object, args }
      return M.SUCCESS_EXIT_CODE, 'successed!'
    end

    local obj = new_obj()

    local opts = {
      context = { id = 1 },
      dispatcher = external_dispatcher_func,
    }

    local editor = M.init(obj, opts)
    local prompt, _ = editor:build_prompt_oneliner()
    local user_input = '1 "-" text nil \\builtin_command arg1 arg2'
    local ok, ret = editor:parse_input(prompt, user_input)
    assert.same({ true, 'successed!' }, { ok, ret })

    local exp_logs = {
      { { id = 1 }, obj, { 'builtin_command', 'arg1', 'arg2' } } }
    assert.same(exp_logs, test_logs)
  end)

  it("dispatcher is table with handlers", function()
    local test_logs = {}

    local external_dispatcher = {
      command1 = function(ctx, obj, args)
        test_logs[#test_logs + 1] = { ctx, obj, args }
        return M.SUCCESS_EXIT_CODE, 'successed!'
      end
    }

    local obj = new_obj()

    local opts = {
      context = { id = 1 },
      dispatcher = external_dispatcher,
    }

    local editor = M.init(obj, opts)
    local prompt, _ = editor:build_prompt_oneliner()
    local user_input = '1 "-" text nil \\command1 arg1 arg2'
    local ok, ret = editor:parse_input(prompt, user_input)
    assert.same({ true, 'successed!' }, { ok, ret })

    local exp_logs = {
      { { id = 1 }, obj, { 'command1', 'arg1', 'arg2' } } }
    assert.same(exp_logs, test_logs)
  end)

  it("dispatcher is table with handlers error - no responce", function()
    local external_dispatcher = {
      ---@diagnostic disable-next-line: unused-local
      command1 = function(ctx, object, args)
        -- return M.SUCCESS_EXIT_CODE, 'successed!' -- error!
      end
    }

    local obj = new_obj()

    local opts = {
      context = { id = 1 },
      dispatcher = external_dispatcher,
    }

    local editor = M.init(obj, opts)
    local prompt, _ = editor:build_prompt_oneliner()
    local user_input = '1 "-" text nil \\command1 arg1 arg2'

    assert.match_error(function()
      editor:parse_input(prompt, user_input)
    end, 'Seems the dispatcher is not returning any response')
  end)
end)
