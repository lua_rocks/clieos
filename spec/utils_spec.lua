-- 09-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local clieos = require "clieos"
local M = clieos.UTILS

describe("clieos", function()
  it("is_tbl_equal", function()
    local f = M.is_tbl_equal
    assert.same(true, f({}, {}))
    assert.same(true, f({ 1 }, { 1 }))
    assert.same(true, f({ 2, 1 }, { 2, 1 }))
    assert.same(false, f({ 2, 1 }, { 2, 1, 0 }))
    assert.same(false, f({}, { 2, 1, 0 }))
    assert.same(false, f({ 2, 1, 0 }, { 2, 1 }))
    assert.same(false, f({ 2, 1 }, { '2', 1 }))
    assert.same(true, f({ k = 2, key = 1 }, { k = 2, key = 1 }))
    assert.same(true, f({ k = { a = 2 }, key = 1 }, { k = { a = 2 }, key = 1 }))
    -- deadlock?
    local t1 = {}
    t1.a = t1
    local t2 = {}
    t2.a = t2
    assert.same(false, f(t1, t2))
  end)

  it("is_equal", function()
    local f = M.is_equal
    assert.same(true, f(1, 1))
    assert.same(false, f(1, 2))
    assert.same(false, f(1, '1'))
    assert.same(true, f({}, {}))
    assert.same(false, f({}, nil))
    assert.same(false, f(nil, {}))
    assert.same(true, f({ k1 = 'a' }, { k1 = 'a' }))
    assert.same(false, f({ k1 = 'a' }, { k1 = 'b' }))
  end)

  it("funOrDef", function()
    local f = M.funOrDef
    local module = {
      some_func = function() return 16 end
    }
    local v = f(1, module, 'some_func', nil)
    assert.is_function(v)
    assert.same(16, v())
  end)

  it("wrap_value", function()
    local f = M.wrapq_value
    assert.same('nil', f(nil))
    assert.same('+', f('+'))
    assert.same('"+ +"', f('+ +'))
    assert.same('|', f('|'))
    assert.same('0', f(0))
    assert.same('0', f('0'))
    assert.same('"0 0"', f('0 0'))
    assert.same('""', f(''))
    assert.same('" "', f(' '))
    assert.same('"a b"', f('a b'))
    assert.same("'\"a b\"'", f('"a b"'))

    assert.match_error(function()
      f('"a\' b"')
    end, 'Not implemented yet: case lines with both quotes')
  end)

  it("is_simple_keyname", function()
    -- only_letters_and_digits
    local f = M.is_simple_keyname
    assert.same(false, f(''))
    assert.same(false, f(' '))
    assert.same(false, f('a b'))
    assert.same(false, f('a b '))
    assert.same(false, f('a '))
    assert.same(false, f('a-b'))
    assert.same(false, f('a~b'))
    assert.same(true, f('a'))
    assert.same(false, f('0'))
    assert.same(false, f(','))
    assert.same(false, f('0a'))
    assert.same(false, f('key.dot'))
    assert.same(true, f('key_under'))
    assert.same(true, f('_ok'))
  end)

  it("sort_keys", function()
    local f = M.sort_keys
    assert.same({ 'b', 'c', 'x' }, f({ x = 1, b = 2, c = 3 }))
    assert.same({ 'k', 'v', 'w' }, f({ k = 1, v = 2, w = 3 }))
  end)

  it("sort_keys with is_only_digits", function()
    local f = M.sort_keys
    assert.same({ { 1, 2, 3 }, true }, { f({ 'a', 'b', 'c' }) })
    assert.same({ { 'k' }, false }, { f({ k = 'c' }) })
    assert.same({ { 2, 'k', 1 }, false }, { f({ 'a', 'b', k = 'c' }) })
  end)

  it("inspect", function()
    local f, a = M.inspect, assert.same
    local exp = '{_ = 8, k = "value", [key.dot] = 42}'
    assert.same(exp, f({ k = 'value', ['key.dot'] = 42, _ = 8 }))
    assert.same('{"key.dot"}', f({ 'key.dot' }))
    a('{"a", "b", "c"}', f({ 'a', 'b', 'c' }))
    a('{"a", "b", "c"}', f({ [1] = 'a', [2] = 'b', [3] = 'c' }))
    a('{"a", [3] = "b", [4] = "c"}', f({ [1] = 'a', [3] = 'b', [4] = 'c' }))
    a('{"a", "b", [4] = "c"}', f({ [1] = 'a', [2] = 'b', [4] = 'c' }))
  end)

  it("inspect functions hash names ", function()
    local f = M.inspect
    assert.match('function: 0x[%x]+', f(M.stub_func, { hash = true }))
    assert.same('function', f(M.stub_func))

    local names = { [M.stub_func] = 'clieos.Utils.stub_func' }
    local exp = '(function)clieos.Utils.stub_func'
    assert.same(exp, f(M.stub_func, { func_names = names }))

    local exp2 = '(function)?'
    assert.same(exp2, f(function() end, { func_names = names }))
  end)

  it("format_with_got_value", function()
    local f = M.format_with_got_value
    local exp = 'keyname: expected got: 1'
    assert.same(exp, f(1, '%s: expected got: %s', 'keyname'))

    local exp2 = 'keyname: expected got: (%s) 2'
    assert.same(exp2, f(2, '%s: expected got: (%s) %s', 'keyname'))
  end)

  it("format", function()
    local f = M.format
    assert.same('msg abc', f('msg %s', 'abc'))
    assert.same('msg a', f('msg %s', 'a'))
    assert.same('msg {k = "v"}', f('msg %s', { k = 'v' }))
    assert.same('msg a {k = "v"}', f('msg %s %s', 'a', { k = 'v' }))
    assert.same('msg {k = "v"}', f('msg', { k = 'v' }))
    assert.same('msg abc', f('msg', 'abc'))
    assert.same('msg abc 0', f('msg', 'abc', 0))
    assert.same('msg abc true 1', f('msg', 'abc', true, 1))
    assert.same('msg abc nil a', f('msg', 'abc', nil, 'a'))
    assert.same('msg nil', f('msg %s', nil))
    assert.same('msg nil', f('msg %s', 'nil'))
    assert.same('nil', type(nil))
  end)


  it("split", function()
    assert.same({ 'abc', 'de' }, M.split("abc\nde", "\n"))
  end)

  it("slist2str", function()
    local exp = "{\n[1] = 'abc',\n[2] = 'de',\n}"
    assert.same(exp, M.slist2str({ [1] = 'abc', [2] = 'de' }, ""))
  end)

  it("get_line_n_command", function()
    local f = function(s, sep)
      local l, a = M.get_line_n_command(s, sep)
      return tostring(l) .. '|' .. table.concat(a or {}, '|')
    end
    assert.same('some text |cmd', f("some text \\cmd"))
    assert.same('|cmd', f("\\cmd"))
    assert.same(' |cmd', f(" \\cmd"))

    assert.same('a |cmd|a1|a2|a3', f("a \\cmd,a1,a2,,a3"))
    assert.same('a |cmd|a_1|a2;|a3.', f("a \\cmd,a_1,a2;,,a3."))

    assert.same('a |cmd|a_1|a2;|a3.', f("a \\cmd a_1 a2;  a3.", " "))
    --
    assert.same('a \\nSomething|', f("a \\nSomething", " "))
    assert.same('a \\nSomething |cmd|a1', f("a \\nSomething \\cmd a1", " "))
  end)

  it("get_placeholders_cnt", function()
    local f = M.get_placeholders_cnt
    assert.same(1, f('%s'))
    assert.same(2, f('%s%s'))
    assert.same(2, f('some:%s another:%s'))
    assert.same(3, f('some:%s another:%s and %% 3:%d'))
    assert.same(3, f('some:%s another:%s and % 3:%d'))
    assert.same(4, f('some:%s another:%s and %%%s 4:%d'))
  end)
end)


describe("clieos testing", function()
  local T = clieos.TESTING

  it("mkpatch for given obj", function()
    local obj = { x = 1, y = 2, color = '-' }
    local modt = { x = 3, y = 4, color = '@' }

    local patch, ordered_keys = T.mk_patch(obj, modt) -- workload

    local exp_patch = {
      x = { old = 1, new = 3 },
      y = { old = 2, new = 4 },
      color = { old = '-', new = '@' }
    }
    assert.same(exp_patch, patch)
    local exp_keys = {
      { k = 'color', t = 'string' },
      { k = 'x',     t = 'number' },
      { k = 'y',     t = 'number' }
    }
    assert.same(exp_keys, ordered_keys)
  end)

  it("get_kvpair ignore one key", function()
    local orderedkeys_entry = { x = 'number', _def = 0 }
    local f = M.get_kvpair
    assert.same({ 'x', 'number' }, { f(orderedkeys_entry, '_def') })
  end)

  it("wrapq_value", function()
    local f = M.wrapq_value
    assert.same('"a b"', f("a b", true))
    assert.same('9', f("9", true))
    assert.same('with\\nnewlines', f("with\nnewlines", true))
  end)

  it("str2value", function()
    local f = clieos.Editor.str2value
    assert.same('"a b"', f('"a b"', 'string', 'key'))
    assert.same('9', f("9", 'string', 'key'))
    assert.same("with\nnewlines", f("with\nnewlines", 'string', 'key'))
  end)

  it("back_indexof", function()
    local f = M.back_indexof
    assert.same(3, f("abc", "c"))
    assert.same(2, f("abc", "bc"))
    assert.same(1, f("abc", "abc"))
    assert.same(-1, f("abc", "abce"))
    assert.same(4, f("abc\\nde", "\\n"))
    assert.same(4, f("abc\\nde", "\\"))
  end)

  it("indexof_command", function()
    local f = M.indexof_command
    local line = "def \\cmd arg1"
    assert.same(6, f(line))
    assert.same('cmd', line:sub(6, 8))
    --
    assert.same(6, f("def \\cmd some\\ntext"))
    assert.same(9, f("def \\n \\cmd some\\ntext"))
    assert.same(9, f("def \\n \\cmd some\\ntext\\nwith\\nnewlines"))
  end)
end)
