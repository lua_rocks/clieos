-- 12-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local clieos = require "clieos"
local M = clieos
local P = clieos.Patch

-- example of how to use clieos.Editor to update the object by the user input
-- edit object as one transaction

describe("clieos", function()
  -- constructor of the editable object
  local function new_obj()
    local obj = {
      sub1 = { -- field1
        a = 1,
        b = false,
      },
      sub2 = {
        c = 'text',
        d = nil,
      },

      [M.METHOD_NAME_ORDERED_KEYS] = function() -- orderedkeys
        return { { a = 'num' }, { b = 'bool' }, { c = 'str' }, { d = 'str' } }
      end,

      -- note return phantom keys not a direct fields in self table
      [M.METHOD_NAME_SERIALIZE] = function(self) -- serialize
        return {
          a = self.sub1.a, b = self.sub1.b, c = self.sub2.c, d = self.sub2.d
        }
      end,

      -- method to update self by provided patch
      [M.METHOD_NAME_APPLY_PATCH] = function(self, patch) -- edit
        self.sub1.a = P.update_one_out(patch, 'a', self.sub1.a)
        self.sub1.b = P.update_one_out(patch, 'b', self.sub1.b)
        self.sub2.c = P.update_one_out(patch, 'c', self.sub2.c)
        self.sub2.d = P.update_one_out(patch, 'd', self.sub2.d or M.NIL)

        -- mark as updated vithout validating patch-obj sync
        P.report_set_updated_risky(patch, 'a', 'b', 'c', 'd')

        return P.updated_cnt(patch), patch
      end
    }
    return obj
  end

  ----------------------------------------------------------------------

  it("workflow", function()
    local obj = new_obj()

    local exp_obj_before_edit = {
      sub1 = { a = 1, b = false },
      sub2 = { c = 'text', d = nil },
      [M.METHOD_NAME_SERIALIZE] = obj[M.METHOD_NAME_SERIALIZE],
      [M.METHOD_NAME_ORDERED_KEYS] = obj[M.METHOD_NAME_ORDERED_KEYS],
      [M.METHOD_NAME_APPLY_PATCH] = obj[M.METHOD_NAME_APPLY_PATCH],
    }
    assert.same(exp_obj_before_edit, obj)

    -- edit transaction
    local opts = {}
    local editor = M.init(obj, opts)

    local prompt, line = editor:build_prompt_oneliner()
    assert.same('1 - text nil', line)

    local updated_line_by_user = '42 + TEXT something'

    local ok, patch = editor:parse_input(prompt, updated_line_by_user)
    assert.same(true, ok)

    local exp_patch = {
      a = { new = 42, old = 1 },
      c = { new = 'TEXT', old = 'text' },
      b = { new = true, old = false },
      d = { new = 'something', old_nil = true },
    }
    assert.same(exp_patch, patch)

    local updated, patch0 = editor:apply_patch()
    assert.same({ true, exp_patch }, { updated, patch0 })

    local exp_patch_state = {
      updated = { a = 1, d = 1, c = 1, b = 1 },
      rules = {}
    }
    assert.same(exp_patch_state, patch0:get_state())
    assert.same(4, patch0:updated_cnt())
    assert.equal(patch, patch0) -- one instance

    local exp_obj_updated = {
      sub1 = { a = 42, b = true },
      sub2 = { c = 'TEXT', d = 'something' },
      [M.METHOD_NAME_SERIALIZE] = obj[M.METHOD_NAME_SERIALIZE],
      [M.METHOD_NAME_ORDERED_KEYS] = obj[M.METHOD_NAME_ORDERED_KEYS],
      [M.METHOD_NAME_APPLY_PATCH] = obj[M.METHOD_NAME_APPLY_PATCH],
    }
    assert.same(exp_obj_updated, obj)
  end)

  it("edit_transaction success", function()
    local logs = {}

    local obj = new_obj()

    local ok, ret = M.edit_transaction(obj, {
      ---@param prompt string
      ---@param line string
      ---@return string
      ui_read_line = function(prompt, line)
        logs[1] = { prompt, line }
        -- here we read a line from the user, displaying the keys(prompt) and
        -- current values of the object's fields(line)
        return '42 + TEXT something'
      end,
    })

    assert.same(true, ok)
    local exp_patch = {
      a = { new = 42, old = 1 },
      c = { new = 'TEXT', old = 'text' },
      b = { new = true, old = false },
      d = { new = 'something', old_nil = true },
    }
    assert.same(exp_patch, ret)
    assert.same({ 'a b c d', '1 - text nil' }, logs[1])
  end)
end)
