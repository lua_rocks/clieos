-- 09-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local M = require "clieos"
local U = M.UTILS
local P = M.Patch
local inspect = U.inspect

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


local DEF_NAMES = {
  orderedkeys = M.METHOD_NAME_ORDERED_KEYS,
  serialize = M.METHOD_NAME_SERIALIZE,
  apply_patch = M.METHOD_NAME_APPLY_PATCH,
}

describe("clieos utils", function()
  it("call_getter & call_setter", function()
    local obj = {
      fieldName = { 'a', 'b', 'c' },
      field2 = 42,
      getFieldName = function(self)
        return table.concat(self.fieldName, ' ')
      end,
      setFieldName = function(self, value)
        self.fieldName = U.split(value, ' ')
      end
    }
    assert.same({ true, 'a b c' }, { M.call_getter(obj, 'fieldName') })
    assert.same({ false }, { M.call_getter(obj, 'field2') })
    assert.same(true, M.call_setter(obj, 'fieldName', 'd e f'))
    assert.same(false, M.call_setter(obj, 'field2', 88))
    local exp = { 'd', 'e', 'f' }
    assert.same(exp, obj.fieldName)
  end)
end)


--

describe("clieos.Editor", function()
  --
  it("get_orderedkeys", function()
    local obj = {
      x = 42,
      orderedkeys = function() return { { x = 'number' } } end,
    }

    local e = M.Editor:new({ obj = obj })
    assert.same({ { k = 'x', t = 'number' } }, e:get_orderedkeys())
  end)

  it("get_orderedkeys", function()
    local obj = {
      x = 42,
      orderedkeys = function() return { { k = 'x', t = 'number' } } end,
    }
    local e = M.Editor:new({ obj = obj })
    assert.same({ { k = 'x', t = 'number' } }, e:get_orderedkeys())
  end)

  it("get_orderedkeys", function()
    local obj = {
      x = 42,
      orderedkeys = function() return { { 'x', 'number' } } end,
    }
    local e = M.Editor:new({ obj = obj })
    assert.same({ { k = 'x', t = 'number' } }, e:get_orderedkeys())
  end)

  --

  it("mk_snapshot", function()
    local ret = { x = 42 }
    local trace = {}

    local obj = {
      x = 42,
      orderedkeys = function(self)
        trace[#trace + 1] = fmt('obj:orderedkeys(%s)\n', inspect(self))
        return { { x = 'number' } }
      end,
      serialize = function(self, data)
        local i = inspect
        assert(self, 'instance')
        trace[#trace + 1] = fmt('obj:serialized(%s) => %s\n', i(data), i(ret))
        return ret
      end
    }

    local e = M.Editor:new({ obj = obj }, { data = { 'opts-to-resialize' } })
    assert.same({ 'opts-to-resialize' }, e.serialize_opts)

    assert.same({ { k = 'x', t = 'number' } }, e:get_orderedkeys())
    local exp_log = {
      "obj:orderedkeys({orderedkeys = function, serialize = function, x = 42})\n"
    }
    assert.same(exp_log, trace)

    local exp = { x = { s = '42', o = 42 } }
    assert.same(exp, e:mk_snapshot()) -- call serialized mehod inside
    local exp_log2 = {
      "obj:orderedkeys({orderedkeys = function, serialize = function, x = 42})\n",
      "obj:serialized({\"opts-to-resialize\"}) => {x = 42}\n"
    }
    assert.same(exp_log2, trace)
  end)


  it("new via init no obj:orderedkeys built ordered keylists itself", function()
    local obj = { x = 42, y = 88, c = '@', orderedkeys = nil, serialize = nil }

    local opts = { data = { 'to-serialize' } }
    local editor = M.init(obj, opts)

    local exp_editor_state = {
      obj = { c = '@', x = 42, y = 88 },
      names = DEF_NAMES,
      rules = {},
      serialize_opts = { 'to-serialize' },
      orderedkeys = {
        { k = 'c', t = 'string' },
        { k = 'x', t = 'number' },
        { k = 'y', t = 'number' }
      },
      snapshot = {
        c = { s = '@', o = '@' },
        x = { s = '42', o = 42 },
        y = { s = '88', o = 88 }
      },
    }
    assert.same(exp_editor_state, editor)
  end)


  it("init with normalized orderedkeys in obj", function()
    local obj = {
      a = 1,
      b = false,
      c = 'text',
      d = nil,
      orderedkeys = function()
        return {
          { k = 'a', t = 'number' },
          { k = 'b', t = 'boolean' },
          { k = 'c', t = 'string' },
          { k = 'd', t = 'string' }
        }
      end,
      serialize = function(self)
        return { a = self.a, b = self.b, c = self.c, d = self.d }
      end,
    }
    local opts = { 'empty' }
    local editor = M.init(obj, opts)
    local exp = {
      obj = obj,
      names = DEF_NAMES,
      rules = {},
      orderedkeys = {
        { k = 'a', t = 'number' },
        { k = 'b', t = 'boolean' },
        { k = 'c', t = 'string' },
        { k = 'd', t = 'string' }
      },
      snapshot = {
        a = { s = '1', o = 1 },
        b = { s = '-', o = false },
        c = { s = 'text', o = 'text' },
        d = { s = 'nil', o = nil }, -- without o(original is nil)
      },
    }
    assert.same(exp, editor)
  end)

  it("init with short orderedkeys in obj", function()
    local obj = {
      a = 1,
      b = false,
      c = 'text',
      orderedkeys = function()
        return {
          { 'a', 'number' },
          { 'b', 'boolean' },
          { 'c', 'string' },
        }
      end,
      serialize = function(self)
        return { a = self.a, b = self.b, c = self.c, d = self.d }
      end,
    }
    local opts = { 'empty' }
    local editor = M.init(obj, opts)
    local exp = {
      obj = obj,
      names = DEF_NAMES,
      rules = {},
      orderedkeys = {
        { k = 'a', t = 'number' },
        { k = 'b', t = 'boolean' },
        { k = 'c', t = 'string' },
      },
      snapshot = {
        a = { s = '1', o = 1 },
        b = { s = '-', o = false },
        c = { s = 'text', o = 'text' },
      },
    }
    assert.same(exp, editor)
  end)

  it("build_orderedkeys", function()
    local f = M.Editor.autobuild_orderedkeys
    local obj = { a = 1, w = 2, z = 3, b = 4 }
    local exp = {
      { k = 'a', t = 'number' },
      { k = 'b', t = 'number' },
      { k = 'w', t = 'number' },
      { k = 'z', t = 'number' }
    }
    assert.same(exp, f(obj))
  end)

  --

  it("validate_type", function()
    local f = M.Editor.validate_type
    assert.same('number', f('n'))
    assert.same('string', f('s'))
    assert.same('boolean', f('b'))
    assert.same('table', f('t'))

    assert.same('number', f('num'))
    assert.same('string', f('str'))
    assert.same('boolean', f('bool'))
    assert.same('table', f('tbl'))

    assert.same('number', f('number'))
    assert.same('string', f('string'))
    assert.same('boolean', f('boolean'))
    assert.same('table', f('table'))
  end)

  it("validated_orderedkeys_with_types full form", function()
    local f = M.Editor.validated_orderedkeys_with_types
    local orderedkeys = {
      { k = 'a', t = 'number' }, -- entry 1
      { k = 'b', t = 'number' },
      { k = 'w', t = 'number' },
      { k = 'z', t = 'number' }
    }
    -- ok
    assert.no_error(function()
      assert.same(orderedkeys, f(orderedkeys))
    end)

    assert.no_error(function()
      f({ { a = 'number' } })
    end)

    local err = 'expected not empty, ordered list of entries' ..
        ' like: {k="x", t="number"} or {x="number"}, got: {a = "number"}'

    assert.match_error(function()
      f({ a = 'number' })
    end, err)

    assert.match_error(function()
      f({ 'x', 'y' })
    end, '1: expected table entry of kvpair got: x')

    assert.match_error(function()
      f({ { k = 'a', vtype = 'number' } })
    end, '1: expected .string.type got:nil for key a in {k = "a", vtype = "number"}')
  end)

  it("validated_orderedkeys_with_types short form 1", function()
    local f = M.Editor.validated_orderedkeys_with_types
    local provided_orderedkeys = {
      { 'a', 'number' }, -- entry 1
      { 'b', 'number' },
      { 'z', 'number' }
    }
    local exp_orderedkeys = {
      { k = 'a', t = 'number' },
      { k = 'b', t = 'number' },
      { k = 'z', t = 'number' }
    }
    assert.same(exp_orderedkeys, f(provided_orderedkeys))
  end)

  it("validated_orderedkeys_with_types short form 2", function()
    local f = M.Editor.validated_orderedkeys_with_types
    local provided_orderedkeys = {
      { a = 'number' }, -- entry 1
      { b = 'number' },
      { z = 'number' }
    }
    local exp_orderedkeys = {
      { k = 'a', t = 'number' },
      { k = 'b', t = 'number' },
      { k = 'z', t = 'number' }
    }
    assert.same(exp_orderedkeys, f(provided_orderedkeys))
  end)

  local function new_obj()
    local obj = {
      a = 1,
      b = false,
      c = 'text',
      d = nil,
      orderedkeys = function() -- M.METHOD_NAME_ORDERED_KEYS
        return { { a = 'num' }, { b = 'bool' }, { c = 'str' }, { d = 'str' } }
      end,
      serialize = function(self) -- M.METHOD_NAME_SERIALIZE
        return { a = self.a, b = self.b, c = self.c, d = self.d }
      end,
      [M.METHOD_NAME_APPLY_PATCH] = nil, -- edit
    }
    return obj
  end

  it("build_prompt_oneliner", function()
    local obj = new_obj()
    local opts = { 'empty' }
    local editor = M.init(obj, opts)
    local msg, line = editor:build_prompt_oneliner()
    assert.same({ 'a b c d', '1 - text nil' }, { msg, line })
  end)

  -- worklow from prepare to create patch
  it("parse_input_to_patch unchanged", function()
    local obj = new_obj()
    local opts = { 'empty' }
    local editor = M.init(obj, opts)
    local exp_state_1 = {
      obj = obj,
      names = DEF_NAMES,
      rules = {},
      orderedkeys = {
        { k = 'a', t = 'number' },
        { k = 'b', t = 'boolean' },
        { k = 'c', t = 'string' },
        { k = 'd', t = 'string' }
      },
      snapshot = {
        a = { s = '1', o = 1 },
        b = { s = '-', o = false },
        c = { s = 'text', o = 'text' },
        d = { s = 'nil' },
      },
    }
    assert.same(exp_state_1, editor)

    local prompt, line = editor:build_prompt_oneliner() -- workload

    assert.same({ 'a b c d', '1 - text nil' }, { prompt, line })
    assert.same({ prompt, line }, { editor.prompt, editor.def_values })

    local ok, patch = editor:parse_input_to_patch(prompt, line) -- workload

    assert.same({ false, 'unchanged' }, { ok, patch })
  end)

  it("parse_input_to_patch old&new value is nil", function()
    local obj = new_obj()
    local editor = M.init(obj, {})

    local prompt, line = editor:build_prompt_oneliner() -- workload
    assert.same({ 'a b c d', '1 - text nil' }, { prompt, line })

    local changed_line = '1 + text nil'
    local ok, patch = editor:parse_input_to_patch(prompt, changed_line)
    local exp_patch = {
      a = { old = 1, new = 1 },
      b = { old = false, new = true },
      c = { old = 'text', new = 'text' },
      d = { old_nil = true }, -- old = nil new = nil  !!
    }
    assert.same({ true, exp_patch }, { ok, patch })
  end)

  it("pass serialize_opts to patch inner state", function()
    local editor = M.init(new_obj(), { data = { 'someting' } })
    local msg, _ = editor:build_prompt_oneliner()
    local ok, patch = editor:parse_input_to_patch(msg, '1 "-" UPDATED nil')
    assert.same(true, ok)
    local exp = {
      a = { new = 1, old = 1 },
      c = { new = 'UPDATED', old = 'text' },
      b = { new = false, old = false },
      d = { old_nil = true },
    }
    assert.same(exp, patch)

    assert.same({ 'someting' }, patch:get_data())
  end)

  -- This rule is used when updating values by patch, to prevent setting nil
  -- idea: configure in orderedkeys via nullable field
  it("pass rule can_set_nil", function()
    local obj = new_obj()

    local opts = { can_set_nil = false } -- pass to editor
    local editor = M.init(obj, opts)

    assert.same({ can_set_nil = false }, editor.rules)

    local prompt, _ = editor:build_prompt_oneliner()
    local updated_line_by_user = '42 + TEXT something'
    local ok, patch = editor:parse_input(prompt, updated_line_by_user)

    assert.same({ true, true }, { ok, patch ~= nil }) ---@cast patch table
    local exp = {
      updated = {},
      rules = { can_set_nil = false } -- pass from editor
    }
    assert.same(exp, patch:get_state())
  end)


  -- can be used when all orderedkeys is real fields(keys) of the obj
  -- not suitable when the serialization method produces phantom fields
  it("update_one_in string", function()
    local obj = new_obj()
    assert.same('text', obj.c)
    -- local editor = M.init(obj, {})
    -- local prompt, line = editor:build_prompt_oneliner() -- workload
    -- assert.same({ 'a b c d', '1 "-" text nil' }, { prompt, line })
    -- ok, patch = editor:parse_input_to_patch(prompt, line)

    local patch = P:new({
      a = { old = 1, new = 1 },
      b = { old = false, new = false },
      c = { old = 'text', new = 'updated' },
      d = {}, -- old = nil new = nil  !!
    })
    assert.same(true, P.update_one_in(patch, 'c', obj))
    assert.same('updated', obj.c)
  end)

  it("update_one_out string patch is instance of Patch:new()", function()
    local obj = new_obj() -- editable object with fields: [a b c d]
    assert.same('text', obj.c)
    -- local editor = M.init(obj, {})
    -- local prompt, line = editor:build_prompt_oneliner() -- workload
    -- assert.same({ 'a b c d', '1 "-" text nil' }, { prompt, line })

    local patch = P:new({
      a = { old = 1, new = 1 },
      b = { old = false, new = false },
      c = { old = 'text', new = 'updated' },
      d = {}, -- old = nil new = nil  !!
    })

    local new_value, updated = P.update_one_out(patch, 'c', obj.c)
    assert.same({ 'updated', true }, { new_value, updated })
    assert.same('text', obj.c)
    local exp = { updated = { c = 0 } }
    assert.same(exp, patch:get_state())

    assert.same(1, P.touched_cnt(patch))
    assert.same(0, P.updated_cnt(patch))
  end)

  it("update_one_out string patch is plain table", function()
    local obj = new_obj() -- editable object with fields: [a b c d]
    assert.same('text', obj.c)

    local patch = {
      a = { old = 1, new = 1 },
      b = { old = false, new = false },
      c = { old = 'text', new = 'updated' },
      d = {}
    }
    assert.same({ 'updated', true }, { P.update_one_out(patch, 'c', obj.c) })
    assert.same('text', obj.c)
    local exp = { updated = { c = 0 } }
    assert.same(exp, patch:get_state())

    assert.same(1, P.touched_cnt(patch))
    assert.same(0, P.updated_cnt(patch))
  end)


  it("update_one_out string caller job", function()
    local obj = new_obj() -- editable object with fields: [a b c d]
    assert.same('text', obj.c)

    local patch = {
      a = { old = 1, new = 1 },
      c = { old = 'text', new = 'updated' },
    }

    local new_value, updated = P.update_one_out(patch, 'c', obj.c)
    assert.same({ 'updated', true }, { new_value, updated })
    local old_value = obj.c
    assert.same('text', old_value)

    local exp = { updated = { c = 0 } }
    assert.same(exp, patch:get_state())

    assert.same(1, P.touched_cnt(patch))
    assert.same(0, P.updated_cnt(patch))
    -- job on caller side:
    obj.c = new_value
    patch:report_set_updated('c', old_value, new_value)
    assert.same(0, P.touched_cnt(patch))
    assert.same(1, P.updated_cnt(patch))
  end)


  it("apply_patch direct via obj[key] = ..", function()
    local obj = new_obj() -- editable object with fields: [a b c d]
    assert.is_nil(obj.edit)

    local editor = M.init(obj, {})
    local prompt, line = editor:build_prompt_oneliner()
    assert.same('1 - text nil', line)

    -- human editing here
    local updated_line = '42 + TEXT something'

    local ok, patch = editor:parse_input_to_patch(prompt, updated_line)
    assert.same('clieos.Patch', patch._cname)

    assert.same(true, ok)
    local exp_patch = {
      a = { new = 42, old = 1 },
      c = { new = 'TEXT', old = 'text' },
      b = { new = true, old = false },
      d = { new = 'something', old_nil = true },
    }
    assert.same(exp_patch, patch)

    local updated, patch0 = editor:apply_patch()
    assert.same(true, updated)
    assert.same(exp_patch, patch0)
    assert.is_table(getmetatable(patch0))

    local exp_patch_state = {
      updated = { a = 1, d = 1, c = 1, b = 1 },
      rules = {},
    }
    assert.same(exp_patch_state, patch:get_state())
    assert.same(4, patch:updated_cnt())
  end)


  it("apply_patch obj.apply_patch fail: bad user code", function()
    local obj = new_obj() -- editable object with fields: [a b c d]
    obj.edit = function( --[[self]] _, patch)
      -- bad update user code
      -- here most be used Patch.update_one_out() or
      -- Patch.set_update or Patch.report_set_updated_risky
      return 1, patch
    end

    local editor = M.init(obj, {})
    local prompt, _ = editor:build_prompt_oneliner()
    local updated_line = '42 + TEXT something'

    local ok, _ = editor:parse_input_to_patch(prompt, updated_line)
    assert.same(true, ok)
    local err = "WARNING keys changed:4 but mark as updated only:0\n" ..
        'after applying the patch state:{rules = {}, updated = {}}'
    assert.match_error(function() editor:apply_patch() end, err)
  end)


  -- idea: store the number of requests to the key directly inside
  -- patch_wrapped_value.
  -- to track which keys were requested by user code
  it("apply_patch obj.apply_patch success", function()
    local obj = new_obj() -- editable object with fields: [a b c d]

    ---- user code to update self by incomming patch  ----
    -- inject method to object
    obj[M.METHOD_NAME_APPLY_PATCH] = function(self, patch)
      local a = P.update_one_out(patch, 'a', self.a)
      local b = P.update_one_out(patch, 'b', self.b)
      local c = P.update_one_out(patch, 'c', self.c)
      local d = P.update_one_out(patch, 'd', self.d or M.NIL)
      -- validate or modify state based on updated fields
      local old_a, old_b, old_c, old_d = obj.a, obj.b, obj.c, obj.d
      obj.a, obj.b, obj.c, obj.d = a, b, c, d

      -- mark as updated
      P.report_set_updated(patch, 'a', old_a, a)
      P.report_set_updated(patch, 'b', old_b, b)
      P.report_set_updated(patch, 'c', old_c, c)
      P.report_set_updated(patch, 'd', old_d, d)

      return P.updated_cnt(patch), patch
    end
    ----                                                ----

    local editor = M.init(obj, {})

    local prompt, line = editor:build_prompt_oneliner()
    assert.same('1 - text nil', line)

    local updated_line_by_user = '42 + TEXT something'

    local ok, patch = editor:parse_input_to_patch(prompt, updated_line_by_user)
    assert.same(true, ok)
    local exp_patch = {
      a = { new = 42, old = 1 },
      c = { new = 'TEXT', old = 'text' },
      b = { new = true, old = false },
      d = { new = 'something', old_nil = true }
    }
    assert.same(exp_patch, patch)

    local updated, patch0 = editor:apply_patch() -- call the user code inside
    assert.same({ true, exp_patch }, { updated, patch0 })
    assert.is_table(getmetatable(patch0))

    local exp_patch_state = {
      updated = { a = 1, d = 1, c = 1, b = 1 },
      rules = {},
    }
    assert.same(exp_patch_state, patch:get_state())
    assert.same(4, patch:updated_cnt())
  end)


  it("apply_patch obj.apply_patch success set_updated_risky", function()
    local obj = new_obj() -- editable object with fields: [a b c d]
    -- inject method to object
    obj[M.METHOD_NAME_APPLY_PATCH] = function(self, patch)
      self.a = P.update_one_out(patch, 'a', self.a)
      self.b = P.update_one_out(patch, 'b', self.b)
      self.c = P.update_one_out(patch, 'c', self.c)
      self.d = P.update_one_out(patch, 'd', self.d or M.NIL)

      -- mark as updated vithout validating patch-obj sync
      P.report_set_updated_risky(patch, 'a', 'b', 'c', 'd')

      return P.updated_cnt(patch), patch
    end

    local editor = M.init(obj, {})
    local prompt, line = editor:build_prompt_oneliner()
    assert.same('1 - text nil', line)
    local updated_line_by_user = '42 + TEXT something'

    local ok, patch = editor:parse_input_to_patch(prompt, updated_line_by_user)
    assert.same(true, ok)
    local exp_patch = {
      a = { new = 42, old = 1 },
      c = { new = 'TEXT', old = 'text' },
      b = { new = true, old = false },
      d = { new = 'something', old_nil = true },
    }
    assert.same(exp_patch, patch)

    local updated, patch0 = editor:apply_patch()
    assert.same({ true, exp_patch }, { updated, patch0 })

    local exp_patch_state = {
      updated = { a = 1, d = 1, c = 1, b = 1 },
      rules = {},
    }
    assert.same(exp_patch_state, patch:get_state())
    assert.same(4, patch:updated_cnt())
  end)


  it("update_all desynchronized", function()
    local obj = new_obj() -- editable object with fields: [a b c d]

    local editor = M.init(obj, {})
    local prompt, _ = editor:build_prompt_oneliner()
    local updated_line = '42 + TEXT something'
    local ok, patch = editor:parse_input(prompt, updated_line)
    assert.same(true, ok)
    local exp_patch = {
      a = { new = 42, old = 1 },
      c = { new = 'TEXT', old = 'text' },
      b = { new = true, old = false },
      d = { new = 'something', old_nil = true },
    }
    assert.same(exp_patch, patch) ---@cast patch table
    assert.same({ updated = {}, rules = {} }, patch:get_state())

    local ok2, patch0 = P.update_all(patch, obj) -- workload
    assert.same(true, ok2)
    assert.equal(patch, patch0)

    local exp = { updated = { a = 1, d = 1, c = 1, b = 1 }, rules = {} }
    assert.same(exp, patch:get_state())
  end)


  it("TOUCHED_KEY, UPDATED_KEY no errors", function()
    local obj = new_obj() -- editable object with fields: [a b c d]
    -- inject method to object
    obj[M.METHOD_NAME_APPLY_PATCH] = function(self, patch)
      self.a = P.update_one_out(patch, 'a', self.a)
      self.b = P.update_one_out(patch, 'b', self.b)
      self.c = P.update_one_out(patch, 'c', self.c)
      self.d = P.update_one_out(patch, 'd', self.d or M.NIL)

      -- mark as updated vithout validating patch-obj sync
      P.report_set_updated_risky(patch, 'a', 'b', 'c', 'd')

      return P.updated_cnt(patch), patch
    end

    local editor = M.init(obj, {})
    local prompt, _ = editor:build_prompt_oneliner()
    local updated_line = '1 - TEXT nil'
    local ok, patch = editor:parse_input(prompt, updated_line)
    assert.same(true, ok)
    local exp_patch = {
      a = { new = 1, old = 1 },
      c = { new = 'TEXT', old = 'text' },
      b = { new = false, old = false },
      d = { old_nil = true },
    }
    assert.same(exp_patch, patch) ---@cast patch table
    local exp = { updated = {}, rules = {} }
    assert.same(exp, patch:get_state())

    ---@diagnostic disable-next-line: redundant-parameter
    obj[M.METHOD_NAME_APPLY_PATCH](obj, patch) -- workload

    local exp_report = {
      updated = {
        a = M.UNCHANGED_KEY,
        b = M.UNCHANGED_KEY,
        c = M.UPDATED_KEY,
        d = M.UNCHANGED_KEY,
      },
      rules = {}
    }
    assert.same(exp_report, patch:get_state())
    local updated = patch:commit()
    assert.same(1, updated)
  end)


  it("TOUCHED_KEY UPDATED_KEY errors", function()
    local obj = new_obj() -- editable object with fields: [a b c d]
    -- inject method to object
    obj[M.METHOD_NAME_APPLY_PATCH] = function(self, patch)
      self.a = P.update_one_out(patch, 'a', self.a)
      self.b = P.update_one_out(patch, 'b', self.b)
      self.c = P.update_one_out(patch, 'c', self.c)
      self.d = P.update_one_out(patch, 'd', self.d or M.NIL)

      -- mark as updated vithout validating patch-obj sync
      P.report_set_updated_risky(patch, 'a', 'b' --[[, 'c', 'd']])

      return P.updated_cnt(patch), patch
    end

    local editor = M.init(obj, {})
    local prompt, _ = editor:build_prompt_oneliner()
    local updated_line = '1 - TEXT nil'
    local ok, patch = editor:parse_input(prompt, updated_line)
    assert.same(true, ok) ---@cast patch table
    assert.same({ updated = {}, rules = {} }, patch:get_state())

    ---@diagnostic disable-next-line: redundant-parameter
    obj[M.METHOD_NAME_APPLY_PATCH](obj, patch) -- workload

    local exp = {
      rules = {},
      updated = {
        c = M.TOUCHED_KEY, -- not marked as updated
      }
    }

    assert.same(exp, patch:get_state())

    local err = [[
WARNING keys changed:1 but mark as updated only:0
after applying the patch state:{rules = {}, updated = {c = 0}}
forgotten keys: {c = {touched = true, w = {new = "TEXT", old = "text"}}}]]

    assert.match_error(function()
      patch:commit()
    end, err)
  end)


  it("is_value_changed", function()
    local obj = new_obj() -- editable object with fields: [a b c d]
    -- inject method to object
    local editor = M.init(obj, {})
    local prompt, _ = editor:build_prompt_oneliner()
    local updated_line = '42 + TEXT something'
    local ok, patch = editor:parse_input(prompt, updated_line)
    assert.same(true, ok) ---@cast patch table
    patch.a = 42 --{ new = 42, old = 1 },

    assert.match_error(function()
      P.is_changed(patch, 'a')
    end, 'a: expected wrapped_value got:42')
  end)


  it("get_forgotten_keys", function()
    local called = false

    local obj = new_obj()
    obj[M.METHOD_NAME_APPLY_PATCH] = function(self, patch)
      called = true
      self.a = P.update_one_out(patch, 'a', self.a)
      self.b = P.update_one_out(patch, 'b', self.b)
      self.c = P.update_one_out(patch, 'c', self.c)
      self.d = P.update_one_out(patch, 'd', self.d or M.NIL)

      -- mark as updated vithout validating patch-obj sync
      -- forgotten to mark all updated keys
      P.report_set_updated_risky(patch, 'a' --[[, 'b', 'c', 'd']])

      return P.updated_cnt(patch), patch
    end

    local editor = M.init(obj, {})
    local prompt, line = editor:build_prompt_oneliner()
    assert.same('1 - text nil', line)

    local user_input = '4210 + TE XT'
    local ok, patch = editor:parse_input_to_patch(prompt, user_input)
    assert.same(true, ok)
    ---@cast patch table

    local err = [[
WARNING keys changed:4 but mark as updated only:1
after applying the patch state:{rules = {}, updated = {a = 1, b = 0, c = 0, d = 0}}
forgotten keys: ]] -- ...
    assert.match_error(function()
      editor:apply_patch()
    end, err)

    local exp_forgotten = {
      b = { w = { new = true, old = false }, touched = true },
      c = { w = { new = 'TE', old = 'text' }, touched = true },
      d = { w = { new = 'XT', old_nil = true }, touched = true }
    }
    assert.same(exp_forgotten, P.get_forgotten_keys(patch))
    assert.same(true, called)
  end)


  it("get_forgotten_keys in error msg in edit_transaction", function()
    local called = false

    local obj = new_obj()
    obj[M.METHOD_NAME_APPLY_PATCH] = function(self, patch)
      called = true
      self.a = P.update_one_out(patch, 'a', self.a)
      self.b = P.update_one_out(patch, 'b', self.b)
      self.c = P.update_one_out(patch, 'c', self.c)
      self.d = P.update_one_out(patch, 'd', self.d or M.NIL)

      -- mark as updated vithout validating patch-obj sync
      -- forgotten to mark all updated keys
      P.report_set_updated_risky(patch, 'a' --[[, 'b', 'c', 'd']])

      return P.updated_cnt(patch), patch
    end

    local opts = {
      ---@diagnostic disable-next-line: unused-local
      ui_read_line = function(msg, input)
        called = true
        return '4210 + TE XT' -- user_input
      end
    }

    local err =
        "WARNING keys changed:4 but mark as updated only:1\n" ..
        'after applying the patch state:' ..
        "{rules = {}, updated = {a = 1, b = 0, c = 0, d = 0}}\n" ..
        "forgotten keys: {" ..
        "b = {touched = true, w = {new = true, old = false}}, " ..
        "c = {touched = true, w = {new = \"TE\", old = \"text\"}}, " ..
        "d = {touched = true, w = {new = \"XT\", old_nil = true}}}"

    assert.match_error(function()
      --[[local ok, patch = ]]
      M.edit_transaction(obj, opts)
    end, err)

    assert.same(true, called)
  end)


  it("bad type expected number got string", function()
    local obj = new_obj()

    local ok, patch = M.edit_transaction(obj, {
      ui_read_line = function(_, _)
        return 'not-a-number - text nil' -- user_input
      end
    })

    local exp = {
      false,
      "key:'a' expected value with type: number, got: nil not-a-number"
    }
    assert.same(exp, { ok, patch }) ---@cast patch table
  end)

  it("readable_errors not-a-number", function()
    local obj = new_obj()
    local editor = M.init(obj, {})

    local prompt, _ = editor:build_prompt_oneliner()
    local user_input = 'not-a-number - text nil'

    local ok, patch = editor:parse_input(prompt, user_input)
    assert.same(true, ok)
    local exp_patch = {
      a = { new = 1, old = 1 }, -- error here
      b = { new = false, old = false },
      c = { new = 'text', old = 'text' },
      d = { old_nil = true },
    }
    assert.same(exp_patch, patch)

    local errors = editor:build_readable_errors()
    local exp = "key:'a' expected value with type: number, got: nil not-a-number"
    assert.same(exp, errors)
  end)


  -- todo
  it("readable_errors not-a-bool", function()
    local obj = new_obj()
    local editor = M.init(obj, {})

    local prompt, _ = editor:build_prompt_oneliner()
    local user_input = '1 "not-a-bool" text nil'

    local ok, patch = editor:parse_input(prompt, user_input)
    assert.same(true, ok)
    local exp_patch = {
      a = { new = 1, old = 1 },
      b = { new = false, old = false }, -- todo
      c = { new = 'text', old = 'text' },
      d = { old_nil = true },
    }
    assert.same(exp_patch, patch)

    local errors = editor:build_readable_errors()
    local exp = "key:'b' expected value with type: boolean, got: nil not-a-bool"
    assert.same(exp, errors)
  end)

  -- if in stage of build snapshot of curren object state value for one
  -- specified orderedkey is not provided take it from _def(ault value)
  it("default value", function()
    local obj = {
      a = nil,
      x = 1,
      orderedkeys = function( --[[self]])
        return {
          { a = 'number',     _def = 0 },
          { flag = 'boolean', _def = false },
          { x = 'number' },
        }
      end,
      serialize = function(self, _ --[[data]])
        return { a = self.a, x = self.x }
      end
    }
    local editor = M.init(obj, {})
    local prompt, input = editor:build_prompt_oneliner()
    assert.same({ 'a flag x', '0 - 1' }, { prompt, input })
  end)

  --

  it("default value is a function", function()
    local called = false

    local def_value_cb = function(_ --[[self]], key)
      called = true
      if key == 'flag' then return true end
    end

    local obj = {
      a = nil,
      x = 1,

      orderedkeys = function( --[[self]])
        return {
          { a = 'number',     _def = 0 },
          { flag = 'boolean', _def = def_value_cb },
          { x = 'number' },
        }
      end,
      serialize = function(self, _ --[[data]])
        return { a = self.a, x = self.x }
      end
    }
    local editor = M.init(obj, {})
    local prompt, input = editor:build_prompt_oneliner()
    assert.same({ 'a flag x', '0 + 1' }, { prompt, input })
    assert.is_true(called)
  end)
end)
