-- 09-05-2024 @author Swarg
--
-- Goal:
--  - serializes the object into a string for manual human editing via cli
--  - applay changes to object with report
--
-- how its works:
-- - request from the object or build itself a sorted list of keys and their types
-- - serialize object state to display as plain text
-- - remembering the types of each property field of an object (table)
-- - formation of a one-liner for human editing
-- - parsing human input(text) back into values of appropriate types.
-- - applying changes to the object(table)
-- - provide a report on the work done

local M = {}
M._VERSION = 'clieos 0.6.1'
M._URL = 'https://gitlab.com/lua_rocks/clieos'

---@class clieos.Editor
---@field new fun(self, o:table?, opts:table?): clieos.Editor
---@field names table function callback names(serialize, orderedkeys)
---@field rules table{can_set_nil:boolean?}
---@field obj table
---@field serialize_opts table? -- used in mk_snapshot stage
---@field direct boolean
---@field orderedkeys table -- {{k = name, t = type}, { k, t }, ...}
---@field snapshot table { k = { v = 'value'} }}
---@field dispatcher table?|function -- for built-in commands direcly during editing
---@field context table? --  used to run built-in commands
---@field prompt string?
---@field def_values string? -- editable line with values to edit synced with prompt(ordered keys)
---@field patch table?
---@field ui_read_line function?
local C = {
  _cname = 'clieos.Editor'
}

---@class clieos.Patch
---@field new fun(self, o:table?, data?): clieos.Patch
---@field get_rules fun(self): table
---@field rules2 fun(self): table
local P = {
  _cname = 'clieos.Patch'
}


local U, T = {}, {}
M.Editor = C
M.Patch = P
M.UTILS = U
M.TESTING = T

-- attach to used logger if has
---@diagnostic disable-next-line: undefined-field
local logger_module_name = _G.APP_LOGGER or 'alogger'

--------------------------------------------------------------------------------
-- config

-- Optional API for editle objects
M.METHOD_NAME_ORDERED_KEYS = 'orderedkeys'
M.METHOD_NAME_SERIALIZE = 'serialize'
M.METHOD_NAME_APPLY_PATCH = 'edit'

M.CANCEL_ALIAS = 'q'
M.NIL = { 0 }
M.TOUCHED_KEY = 0
M.UPDATED_KEY = 1
M.TYPE_MISMATCH = -1
M.SUCCESS_EXIT_CODE = 0
M.TABLE_REF = '::TABLE::'

--------------------------------------------------------------------------------

local E, v2s, fmt = {}, tostring, string.format
local log_debug, log_trace

local BT = "::BACKTRACE::"

U.stub_func = function() end

function U.is_table(v) return type(v) == 'table' end

function U.is_string(v) return type(v) == 'string' end

function U.is_function(v) return type(v) == 'function' end

---@diagnostic disable-next-line: unused-local
local is_function, is_table, is_string = U.is_function, U.is_table, U.is_string
local fstub = U.stub_func

---@param flag boolean|number|nil
---@param module table?
---@param name string?
---@param def function?|any
function U.funOrDef(flag, module, name, def)
  if flag and type(module) == 'table' then
    assert(type(name) == 'string', 'name')
    local v = module[name]
    if is_function(v) then
      return v
    end
  end
  return def
end

local funOrDef = U.funOrDef


do
  -- plug optional dependencies
  -- local log = require 'alogger'
  local ok, logger = pcall(require, logger_module_name)
  log_debug = funOrDef(ok, logger, 'debug', fstub)
  log_trace = funOrDef(ok, logger, 'trace', fstub)
  -- TRACE = 0, DEBUG = 1, INFO = 2, WARN = 3, ERROR = 4, FATAL = 5,
end

--------------------------------------------------------------------------------
--                                 UTILS
--------------------------------------------------------------------------------

---@param a any
---@param b any
---@return boolean
function U.is_equal(a, b)
  if type(a) == 'table' and type(b) == 'table' then
    return U.is_tbl_equal(a, b)
  else
    return a == b
  end
end

---@param a table
---@param b table
---@return boolean
function U.is_tbl_equal(a, b, passed)
  passed = passed or {}
  passed[a], passed[b] = true, true

  for k, av in pairs(a) do
    local bv = b[k]
    local is_eq = false
    if type(bv) == 'table' and type(av) == 'table' then
      if not passed[bv] and not passed[av] then
        is_eq = U.is_tbl_equal(av, bv, passed)
      else
        return false -- ? anti stack overflow
      end
    else
      is_eq = av == bv
    end
    if not is_eq then
      return false
    end
  end

  -- if b has not existed in a key - not equal
  for k, _ in pairs(b) do if a[k] == nil then return false end end

  return true
end

--
---@param map table|any
function U.key_count(map)
  if is_table(map) then
    local c = 0
    for _, _ in pairs(map) do c = c + 1 end
    return c
  end
  return -1 -- not a table
end

-- pick first pair of key value
-- { key = 'value' }
---@return string? key
---@return any value
---@param ignored_key string?
function U.get_kvpair(map, ignored_key)
  if is_table(map) then
    for k, v in pairs(map) do
      if k ~= ignored_key then
        return k, v
      end
    end
  end
  return nil, nil
end

---@param map table
---@param compare function?
---@return table
---@return boolean only-digits-keys
function U.sort_keys(map, compare)
  local list = {}
  local only_digits = true -- to track is this simple list from 1 to N

  for k, _ in pairs(map) do
    list[#list + 1] = k

    if only_digits and type(k) ~= 'number' then
      only_digits = false
    end
  end

  compare = compare or function(a, b)
    if type(a) == type(b) then
      return a < b
    end
    return 0
  end

  table.sort(list, compare)

  return list, only_digits
end

--
---@param v string?|any
function U.is_simple_keyname(v)
  if type(v) == 'string' then
    return string.match(v, '^[%a_][%a%d_]*$') ~= nil
  end
  return false
end

--
-- put value in quotes for complex strings
--
---@param v any
---@return string
---@param quiet boolean?
function U.wrapq_value(v, quiet)
  if type(v) == 'number' then
    return tostring(v)
  end

  v = v2s(v)
  if v == '' then
    return '""'
  end

  local q, hdq, hsq, hsp = '', false, false, true
  -- newlines
  if string.find(v, "\n") then v = v:gsub("\n", '\\n') end

  hdq = string.find(v, '"', 1, true) ~= nil
  hsq = string.find(v, "'", 1, true) ~= nil
  -- simple = tonumber(v) ~= nil or U.is_simple_keyname(v)
  hsp = string.find(v, " ", 1, true) ~= nil

  if hsp then
    if not hdq then
      q = '"'
    elseif not hsq then
      q = "'"
    else
      if not quiet then
        error('Not implemented yet: case lines with both quotes')
      end
      -- todo escape
    end
  end
  v = q .. v .. q

  return v
end

-- convert value to strnig representation for human editing
---@return any, string
function U.as_string(v)
  local vtype = type(v)
  if vtype == 'boolean' then
    v = v and '+' or '-'
  elseif vtype == 'nil' then
    v = 'nil'
  elseif vtype == 'number' then
    v = v2s(v)
  elseif vtype == 'string' then
    v = v2s(v)
    -- wrap to quotes
    -- if v == '' then v = '""' end
  elseif vtype == 'table' then
    v = M.TABLE_REF
  end

  return v, vtype
end

-- dump object value to string
-- tables prints in simple one-line style without newlines
---@param opts table|nil
function U.inspect(obj, opts)
  local vt = type(obj)
  if vt == 'table' then
    opts = opts or {}
    opts.passed = opts.passed or { counter = 0 }
    local passed = opts.passed

    if passed[obj] then
      return '<table-' .. tostring(passed[obj]) .. '>'
    end

    passed.counter = (passed.counter or 0) + 1
    passed[obj] = passed.counter
    local first, s = true, '{'
    local orderedkeys, only_digits = U.sort_keys(obj, nil)

    local prev_digit_k = 0
    local show_all_digit_keys = opts.show_all_digit_keys or false
    if not show_all_digit_keys and not only_digits then
      show_all_digit_keys = true
    end

    for _, k in pairs(orderedkeys) do
      local eq_sep = ' = '
      local v = obj[k]
      if not first then
        s = s .. ', '
      end
      local kt = type(k)
      if kt == 'number' then
        if show_all_digit_keys or prev_digit_k + 1 ~= k then
          k = '[' .. k .. ']'
        else
          k, eq_sep = '', ''
        end
        prev_digit_k = prev_digit_k + 1
        --
      else
        show_all_digit_keys = true
        if kt ~= 'string' then
          k = tostring(k)
        elseif not U.is_simple_keyname(k) then
          k = '[' .. k .. ']'
        end
      end

      s = s .. k .. eq_sep .. U.inspect(v, passed)
      first = false
    end

    return s .. '}'
    --
  elseif vt == 'string' then
    -- trunc too big strings?
    return '"' .. obj .. '"'
    --
  elseif vt == 'function' then
    local func_names = (opts or E).func_names

    if is_table(func_names) then
      return '(function)' .. tostring(func_names[obj] or '?')
    end

    return (opts or E).hash == true and tostring(obj) or 'function'
  else
    return tostring(obj)
  end
end

function U.get_placeholders_cnt(fmsg)
  local off, cnt, p = 1, 0, nil
  p = 1

  while p do
    p = string.find(fmsg, '%', off, true)
    if p then
      local ch = string.sub(fmsg, p + 1, p + 1)
      if ch ~= '%' and ch ~= ' ' then -- todo one of sdwxlau
        cnt = cnt + 1
      else
        p = p + 1 -- jump over %%
      end
      off = p + 1
    end
  end

  return cnt
end

function U.format(fmsg, ...)
  fmsg = fmsg or ''
  local off = 1
  local no_places = false
  local i, cnt = 0, select('#', ...)
  while i < cnt do
    i = i + 1
    local val = select(i, ...)
    local t = type(val)
    if t == 'table' then
      if type(val.__tostring) == 'function' then
        val = val.__tostring(val)
      elseif type(val.toString) == 'function' then
        val = val.toString(val)
      else
        val = U.inspect(val)
      end
    elseif t ~= 'string' then
      val = tostring(val)
      -- string special values
    end

    if no_places then
      fmsg = fmsg .. ' ' .. val -- append to end of line of no more  '%s'
    else
      local p, ch
      local search = true
      -- find next %X replace '%%' to '%'
      while search do
        search = false
        p = string.find(fmsg, '%', off, true)
        if p then
          ch = string.sub(fmsg, p + 1, p + 1) -- std ch is: 's' 'd' 'f'...
          if ch == '%' then                   -- %% -> %
            fmsg = string.sub(fmsg, 1, p - 1) .. string.sub(fmsg, p + 1)
            off = p + 1
            search = true
          end
        end
      end
      -- replace %? by value
      if p then
        local left = string.sub(fmsg, 1, p - 1)
        local right = string.sub(fmsg, p + 2)
        fmsg = left .. val .. right
        off = #left + #val
      else
        no_places = true
        fmsg = fmsg .. ' ' .. val
      end
    end
  end
  return fmsg
end

function U.get_fmt_n_key(fmtmsg)
  if type(fmtmsg) == 'string' then
    local name = ''
    -- one word - is key
    if string.match(fmtmsg, '^[%w_%-&]+$') ~= nil then
      name = fmtmsg .. ': '
      fmtmsg = nil
    end
    return fmtmsg, name
  end
  return nil, ''
end

--
-- format message by given pattern and values(...)
-- check is fmt patern has free placeholder and so then append got value to the
-- end of formated message
--
-- used to avoid such wet code:
--
--     some_tbl = 42
--     assert_tbl(t, '%s: expected table got: %s', 'key', some_tbl)
--     --> key: expected table got: 42
--
-- to:
--     some_tbl = "not-a-table"
--     assert_tbl(t, '%s: expected table got: %s', 'key')
--     --> key: expected table got: not-a-table
--
---@return string
function U.format_with_got_value(value, fmtmsg, ...)
  local msg = U.format(fmtmsg, ...)

  local args = select('#', ...)
  local placeholders = U.get_placeholders_cnt(fmtmsg)
  if placeholders > args and string.match(fmtmsg, 'got:[%%%l%(%)%s]*%%s%s*$') then
    local i = string.find(msg, '%%s%s*$', 1, false)
    if i then
      msg = msg:sub(1, i - 1) .. U.inspect(value)
    end
  end
  return msg
end

--------------------------------------------------------------------------------
--                           Assertions
--------------------------------------------------------------------------------

function U.assert_eq(a, b, fmtmsg, ...)
  if a ~= b then
    local msg, name
    fmtmsg, name = U.get_fmt_n_key(fmtmsg)

    if not fmtmsg then
      if is_string(a) then a = '"' .. a .. '"' end -- '0' --> "0"
      if is_string(b) then b = '"' .. b .. '"' end
      msg = U.format('expected equal %sgot: %s & %s', name, a, b)
    else
      msg = U.format(fmtmsg, ...)
    end
    error(msg, 2)
  end
  return true
end

function U.assert_eq_in(a, b, name_a, name_b, context)
  if a ~= b then
    if context == nil or context == '' then context = '?' end
    if name_a == nil or name_a == '' then name_a = 'left' end
    if name_b == nil or name_b == '' then name_b = 'right' end
    if type(a) == 'string' then a = '"' .. a .. '"' end
    if type(b) == 'string' then b = '"' .. b .. '"' end
    error(U.format('%s: expected %s equal %s got: %s != %s',
      context or '', name_a, name_b, a, b))
  end
  return true
end

---@param fmtmsg string?
---@return table
function U.assert_table(v, fmtmsg, ...)
  if type(v) ~= 'table' then
    local msg, name
    fmtmsg, name = U.get_fmt_n_key(fmtmsg)

    if not fmtmsg then
      msg = U.format('%sexpected table got: (%s) %s', name, type(v), v)
    else
      msg = U.format_with_got_value(v, fmtmsg, ...)
    end
    error(msg, 2)
  end
  return v
end

function U.assert_table_not_empty(v, fmtmsg, ...)
  if type(v) ~= 'table' or not next(v) then
    local msg, name
    fmtmsg, name = U.get_fmt_n_key(fmtmsg)

    if not fmtmsg then
      msg = U.format('%sexpected not empty table got: (%s) %s', name, type(v), v)
    else
      msg = U.format_with_got_value(v, fmtmsg, ...)
    end
    error(msg, 2)
  end
  return v
end

function U.assert_table_or_nil(v, fmtmsg, ...)
  if v == nil then
    return nil
  end
  fmtmsg = fmtmsg or 'expected table or nil got:%s' -- (%s) %s
  return U.assert_table(v, fmtmsg, ...)
end

---@param fmtmsg string?
function U.assert_not_nil(v, fmtmsg, ...)
  if v == nil then
    local msg
    if not fmtmsg then
      msg = 'expected not nil, got: nil'
    else
      msg = U.format_with_got_value(v, fmtmsg, ...)
    end
    error(msg, 2)
  end
  return v
end

function U.assert_function(v, fmtmsg, ...)
  if type(v) ~= 'function' then
    local msg, name
    fmtmsg, name = U.get_fmt_n_key(fmtmsg)

    if not fmtmsg then
      msg = U.format('%sexpected function got: (%s) %s', name, type(v), v)
    else
      msg = U.format_with_got_value(v, fmtmsg, ...)
    end
    error(msg, 2)
  end
  return v
end

---@param fmtmsg string?
---@return string
function U.assert_string(v, fmtmsg, ...)
  if type(v) ~= 'string' then
    local msg, name
    fmtmsg, name = U.get_fmt_n_key(fmtmsg)

    if not fmtmsg then
      msg = U.format('%sexpected srting, got: %s', name, v)
    else
      msg = U.format_with_got_value(v, fmtmsg, ...)
    end
    error(msg, 2)
  end
  return v
end

local assert_eq_in = U.assert_eq_in
local assert_tbl = U.assert_table
local assert_tbl_ne = U.assert_table_not_empty
local assert_str = U.assert_string
local assert_fn = U.assert_function
local assert_not_nil = U.assert_not_nil

--------------------------------------------------------------------------------

--
--
local function mk_func_name(pref, key)
  key = v2s(key)
  return v2s(pref) .. string.upper(key:sub(1, 1)) .. key:sub(2)
end

--
-- get value via getter in obj
--
---@param obj table
---@param key string
function M.call_getter(obj, key)
  local getter_name = mk_func_name('get', key)
  local getter = (obj or E)[getter_name]
  if type(getter) == 'function' then
    return true, getter(obj)
  end
  return false, nil
end

--
-- set value via setter in obj
--
---@param obj table
---@param key string
function M.call_setter(obj, key, value)
  local setter_name = mk_func_name('set', key)
  local setter = (obj or E)[setter_name]
  if type(setter) == 'function' then
    setter(obj, value)
    return true
  end
  return false
end

--------------------------------------------------------------------------------
--                               Parser
--------------------------------------------------------------------------------

---
--- Examples:
--- <pre>
---   'aa bcd e'      -->   { 'aa', 'bcd', 'e' }
---   'aa b " " ""'   -->   { 'aa', 'b', ' ', '' }
---   'a\"b "" c \\'  -->   { 'a"b', '', 'c', '\' }
--- </pre>
---
---@return table
function M.cli_parse_line(s)
  local SEP, QUOTE = ' ', '"'
  local t = {}
  if type(s) == 'string' and s ~= '' then
    local i, prev_c, prev_i, open = 0, ' ', 1, false

    local function add_word(pstart, pend, drop_empty)
      if drop_empty and pstart > pend then
        return
      end
      local word = s:sub(pstart, pend)
      t[#t + 1] = word:gsub('\\"', '"')
      return word
    end

    while i <= #s do
      i = i + 1
      local c = s:sub(i, i)

      if c == SEP and not open then
        if prev_c ~= SEP then
          add_word(prev_i, i - 1)
          -- dvis(s, prev_i, i, 'SEP prev_c:', prev_c, 'add word', word)
        end
        prev_i = i + 1
      elseif c == QUOTE then
        if not open then
          if prev_c == '\\' then
            -- irnore \"
          else
            -- dvis(s, prev_i, i, 'OPEN')
            if prev_c ~= SEP then
              add_word(prev_i, i - 1)
            end
            prev_i = i + 1
            open = true
          end
        elseif open then
          add_word(prev_i, i - 1)

          prev_i, c = i + 1, SEP -- c will be used in prev_c = c
          open = false
          -- dvis(s, prev_i, i, 'CLOSE add word:', word, 'prev_c:', prev_c)
        end
      end
      prev_c = c
    end

    add_word(prev_i, #s, true)
  end

  return t
end

--------------------------------------------------------------------------------

--
-- Goal: providing a way to stop editing multiple elements via an additional
-- command at the end of the line. like: "some-default date \q"
--
---@param input_line string?
---@return string?
function U.get_lineends_command(input_line)
  if type(input_line) == 'string' and input_line ~= '' then
    local cmd = string.match(input_line, '(\\%w+)$')
    return cmd
  end
  return nil
end

--
-- byte-by-byte
--
---@param needle string
---@param haystack string
---@param offset number? defautl is #needle
---@return number
function U.back_indexof(haystack, needle, offset)
  if needle and haystack and #needle <= #haystack then
    local t = { needle:byte(1, #needle) }
    local mi = #t
    offset = offset or #haystack
    for i = offset, 1, -1 do
      local b = haystack:byte(i, i)
      if b == t[mi] then
        if mi == 1 then
          return i -- found!
        end
        mi = mi - 1
      end
    end
  end
  return -1
end

--
-- find the beginning of the command from the end of the given line
-- ignoring the \\n to use it as newlines not as the commands
--
---@param line string
---@return number
function U.indexof_command(line)
  local p = #line
  while p > 1 do
    local i = U.back_indexof(line, '\\', p)
    if not i then break end

    if line:sub(i + 1, i + 1) ~= 'n' then -- ignore "\n" newlines
      return i + 1
    end
    p = i - 2
  end
  return -1
end

-- "some text \r"            ->  "some text" "\r"
-- "some text \r,arg1,arg2"  ->  "some text" "\r" {'arg1', 'arg2'}
--
-- WARN: you cannot use \n as the command, it used to represent newlines.
--
---@param input_line string
---@param sep string? [, or space]
---@return string text
---@return table? args command + args
function U.get_line_n_command(input_line, sep)
  sep = sep or ','
  if type(input_line) == 'string' and input_line ~= '' then
    local i = U.indexof_command(input_line)

    if i > 0 then
      local line = i > 1 and input_line:sub(1, i - 2) or ''
      local fullcmd, args = input_line:sub(i), {}

      for str in string.gmatch(fullcmd, '([^' .. sep .. ']+)') do
        table.insert(args, str)
      end

      return line, args
    end
    return input_line, nil
  end
  return input_line
end

--------------------------------------------------------------------------------


---@return table
---@param s string
---@param sep string? def is space
---@param t table? output table
function U.split(s, sep, t)
  assert(not t or type(t) == 'table')
  if sep == nil or sep == ' ' then sep = "%s" end

  t = t or {}
  for str in string.gmatch(s, '([^' .. sep .. ']+)') do
    table.insert(t, str)
  end

  return t
end

---@param t table
---@param tab string?
function U.slist2str(t, tab)
  tab = tab or ''
  local s = "{\n"
  for i, cname in ipairs(t) do
    s = s .. fmt("%s[%s] = '%s',\n", tab, i, v2s(cname))
  end
  return s .. (tab:sub(1, #tab - 2) or '') .. "}"
end

--------------------------------------------------------------------------------
--                              Editor
--------------------------------------------------------------------------------

-- supported type names
local TYPES = {
  number = 'number',
  num = 'number',
  n = 'number',

  string = 'string',
  str = 'string',
  s = 'string',

  boolean = 'boolean',
  bool = 'boolean',
  b = 'boolean',

  table = 'table',
  tbl = 'table',
  t = 'table',
}

function C.validate_type(vtype)
  -- todo classes via tables
  local msg = 'supported types: %s; got:%s'
  return assert_not_nil(TYPES[vtype], msg, TYPES, vtype)
end

---@return boolean?
function U.to_bool(s)
  if s == '+' or s == 'true' or s == 'TRUE' or s == 'T' or s == 't' or
      s == '1' or s == 'yes' or s == 'YES' then
    return true
  elseif s == '-' or s == 'false' or s == 'FALSE' or s == 'F' or s == 'f' or
      s == '0' or s == 'no' or s == 'NO' then
    return false
  end
  return nil
end

---@param value string
---@param declared_type string
---@param key string
function C.str2value(value, declared_type, key)
  assert(type(declared_type) == 'string', 'declared_type')
  -- todo classes

  if declared_type == 'boolean' then
    return U.to_bool(value) -- can be nil
    --
  elseif declared_type == 'number' then
    return tonumber(value) -- can be nil
    --
  elseif declared_type == 'string' then
    if value == 'nil' then return nil end
    if value == '""' then return '' end
    return v2s(value):gsub('\\n', "\n")
    --
  elseif declared_type == 'table' then
    error(U.format('Not implemented yet. key:%s type:%s value:%s',
      key, declared_type, value))
  end

  error('unknown type:' .. v2s(declared_type) .. ' for key: ' .. v2s(key))
end

---@param v any
---@return table?|function
function C.validate_cmd_dispatcher(v)
  local t = type(v)
  if not (t == 'nil' or t == 'function' or t == 'table') then
    error(fmt('expected nil|table|function command dispatcher got:%s', v2s(t)))
  end
  return v
end

--
-- automatic construction of an ordered list of keys for a given object (table)
-- in case the object itself does not provide a function to list of fields that
-- can be edited( called METHOD_NAME_ORDERED_KEYS
--
function C.autobuild_orderedkeys(obj)
  local keys = {}

  for key, v in pairs(obj) do
    local value_type = type(v)
    if value_type ~= 'function' then
      keys[#keys + 1] = { k = key, t = value_type }
    end
  end

  table.sort(keys, function(a, b)
    return a.k < b.k
  end)

  return keys
end

---@param obj table
---@param opts table
---@return boolean
---@return string|table|nil
function M.edit_transaction(obj, opts)
  assert_tbl(opts, 'opts')
  assert_fn(opts.ui_read_line, 'ui_read_line')
  -- opts.dispatcher + opts.context

  local editor = M.init(obj, opts)

  local prompt, line = editor:build_prompt_oneliner()

  local user_input = editor.ui_read_line(prompt, line)
  if not user_input or user_input == M.CANCEL_ALIAS or user_input == '' then
    return false, 'canceled'
  end

  local ok, patch, stop = editor:parse_input(prompt, user_input)
  if not ok then
    return false, patch -- errmsg
  end
  if stop then
    return false, patch
  end

  local updated, patch0 = editor:apply_patch()

  return updated, (patch0 or patch)
end

--
-- create new clieos.Editor for given object
-- prepare to edit state of the given object:
--  - obtain ordered key list
--  - create snapshot of the current object state
--
--
---@param obj table
---@param opts table? {data to pass into obj:serialize}
function M.init(obj, opts)
  assert(type(obj) == 'table', 'obj')

  local editor = C:new({ obj = obj }, opts)
  assert_tbl(editor.obj, 'expected table obj got:', editor.obj)

  editor.orderedkeys = editor:get_orderedkeys()

  editor.snapshot = editor:mk_snapshot()

  return editor
end

--
-- constructor
--
---@param o table?
---@param opts table?
---@return clieos.Editor
function C:new(o, opts)
  assert(not o or type(o) == 'table', 'state can be nil or table')
  o = o or {}
  -- 0 is off, 3 is info, 2 is debug
  opts = (opts or E)
  o.serialize_opts = opts.serialize_data or opts.data

  -- o.report_level = opts.report_level or 1
  o.names = o.names or {
    orderedkeys = opts.fname_orderedkeys or M.METHOD_NAME_ORDERED_KEYS,
    serialize = opts.fname_serialize or M.METHOD_NAME_SERIALIZE,
    apply_patch = opts.fname_serialize or M.METHOD_NAME_APPLY_PATCH,
  }
  o.rules = o.rules or {
    can_set_nil = opts.can_set_nil
  }

  o.dispatcher = C.validate_cmd_dispatcher(opts.dispatcher)
  o.context = opts.ctx or opts.context

  o.ui_read_line = opts.ui_read_line

  -- o.prompt, o.def_values, o.patch = nil, nil, nil

  setmetatable(o, self)
  self.__index = self

  return o
end

--
-- request a sorted list of keys from the object itself or
-- create it yourself directly from all available fields of the object
-- (table keys)
--
---@param obj table
---@param methodname string self.names.orderedkeys
---@param serialize_opts table?
---@return table{{k:string, t:string}, {k,t}, ...}
local function get_orderedkeys0(obj, methodname, serialize_opts)
  assert(type(obj) == 'table', 'obj')
  assert(type(methodname) == 'string', 'methodname')

  local f = funOrDef(1, obj, methodname, C.autobuild_orderedkeys)
  assert(is_function(f), 'expected orderedkeys function in obj')

  -- built it myself directly from all available fields of the object
  local auto_built = f == C.autobuild_orderedkeys
  -- self.direct = auto_built

  local raw_orderedkeys = f(obj, serialize_opts)
  local msg = 'expected not empty serialized response from obj got: %s'
  assert_tbl_ne(raw_orderedkeys, msg, raw_orderedkeys)

  local orderedkeys = C.validated_orderedkeys_with_types(raw_orderedkeys)

  log_trace('get_orderedkeys autobuilt:%s %s', auto_built, orderedkeys)
  return orderedkeys
end

-- request a sorted list of keys from the object itself or
-- create it yourself directly from all available fields of the object
-- (table keys)
--
---@return table
function C:get_orderedkeys()
  self.orderedkeys = get_orderedkeys0(
    self.obj, self.names.orderedkeys, self.serialize_opts
  )
  return self.orderedkeys
end

-- todo nullable
-- build normalized orderedkeys entry from variants
---@return string key
---@return string vtype
---@return any default_value
function C.get_kt_fr_raw_orderedkey_entry(e, i)
  i = i or 0
  assert_tbl(e, '%s: expected table entry of kvpair got: %s ', i, e)
  local key, vtype, def --  = e.k, e.t -- {k = 'key_name', t = 'type_name'}

  -- three ways to describe the name of the key (object field) and its type
  if not e.k and not e.t and #e == 2 then -- 1: {'some_key', 'number'}
    key = assert_str(e[1], '%s: expected key at [1] entry:%s', i, e)
    vtype = assert_str(e[2], '%s: expected value at [2] entry:%s', i, e)
  else
    local keys = U.key_count(e)

    if keys == 1 or (keys == 2 and e._def ~= nil) then -- 2: { some_key = 'number' }
      key, vtype = U.get_kvpair(e, '_def')
      def = e._def
      --
    elseif keys >= 2 and keys <= 3 then -- 3: { k = 'some_key', t = 'number'}
      key = assert_str(e.k, '%s: expected (string)key got:%s in %s', i, key, e)
      vtype = assert_str(e.t,
        '%s: expected (string)type got:%s for key %s in %s', i, vtype, key, e)
      def = e.d -- default value if obj:serialize not not provide this one
    else
      error(U.format('%s: unsupport entry structure: %s', i, e))
    end
  end
  ---@cast key string
  return key, vtype, def
end

--
-- validate + build
--
---@param raw table -- raw table to build cooked orderedkeys
---@return table
function C.validated_orderedkeys_with_types(raw)
  assert(type(raw) == 'table' and next(raw), 'expected not empty table')
  -- ignore not number keys -- list expected
  local c = 0
  local t = {}

  for i, e in ipairs(raw) do
    local key, vtype, def = C.get_kt_fr_raw_orderedkey_entry(e, i)
    -- orderedkeys_entry
    t[#t + 1] = { k = key, t = C.validate_type(vtype), d = def }
    c = c + 1
  end

  if c == 0 then
    -- usage example {{x="number"}, {y="number"},..}
    error(U.format('expected not empty, ordered list of entries like:'
      .. ' {k="x", t="number"} or {x="number"}, got: %s', raw))
  end

  return t
end

local function orderedkey_entry_get_kt(e, i)
  assert_tbl_ne(e, '%s:expected table entry got: %s', i, e)
  local key = assert_str(e.k, '%s: expected string key got:%s e:%s', i, e.k, e)
  local t = assert_str(e.t, '%s expected string type of key:%s, got:%s', i, key)
  local def = e.d -- optional
  return key, t, def
end

function C.find_orderedkey_entry(orderedkeys, key)
  for i, e in pairs(orderedkeys) do
    local key0, _ = orderedkey_entry_get_kt(e, i)
    if key == key0 then
      return e
    end
  end
end

--
--
--
--
function C:mk_snapshot() -- obj, orderedkeys, userdata)
  local obj, orderedkeys, data = self.obj, self.orderedkeys, self.serialize_opts
  log_debug('mk_snapshot o:%s keys:%s data:%s', obj, orderedkeys, data)
  assert_tbl_ne(orderedkeys, 'expected orderedkeys')
  local snapshot = {}

  local serialized = nil
  local serialize_fn = funOrDef(1, obj, self.names.serialize, nil)
  if is_function(serialize_fn) then
    serialized = serialize_fn(obj, data)
    local msg = 'expected serialized data (table) from object, got: %s'
    assert_tbl_ne(serialized, msg, serialized)
    -- direct = false
  end

  for i, e in pairs(orderedkeys) do
    local v = nil

    local key, declared_type, def_value = orderedkey_entry_get_kt(e, i)
    assert(snapshot[key] == nil, 'duplicate keys are not allowed')

    if serialized then
      v = serialized[key]
      --
    else -- direct via real fields in obj via getters or direct access to fields
      local ok, val = M.call_getter(obj, key)
      if ok then
        v = val      -- via getter
      else
        v = obj[key] -- direct from object itself
      end
    end

    if v == nil then
      if is_function(def_value) then
        v = def_value(obj, key)
      else
        v = def_value
      end
    end

    local svalue, vtype = U.as_string(v)
    if svalue ~= nil and vtype ~= declared_type and vtype ~= 'nil' then
      local msg = fmt(
        '%s: the declared type:(%s) does not match the actual value: (%s)',
        key, declared_type, vtype)
      error(msg)
      -- todo to report without error
    end

    local wrapped_value = {
      --  "string representation" of the original value
      --  will be used to form the editable string
      s = svalue,

      -- "original value" will be used to check based on parsed user input
      -- if the value has changed during a patch (object update)
      o = v,
      -- type of the value holds in the orderedkeys or in o
    }
    snapshot[key] = wrapped_value
  end

  return snapshot
end

--
--
--
---@return string prompt     - to map with values in def_values_line
---@return string def_values (to edit by human)
function C:build_prompt_oneliner()
  self.prompt, self.def_values, self.patch = nil, nil, nil -- clean

  local msg, def = '', ''                                  -- prompt and def_values
  local wrapq_value = U.wrapq_value
  local snapshot = assert_tbl_ne(self.snapshot, 'snapshot')

  for i, p in pairs(self.orderedkeys) do
    local key = assert_str((p or E).k, '%s: key in orderedkeys.pair %s', i, p)
    local wrapped_value = snapshot[key]
    assert_tbl_ne(wrapped_value, '%s %s: expected wrapped_value got: %s', i, key)

    local svalue = wrapped_value.s -- can be nil

    if msg ~= '' then msg = msg .. ' ' end
    msg = msg .. v2s(key)

    if def ~= '' then def = def .. ' ' end
    def = def .. wrapq_value(svalue)
  end

  -- message(ordered keys) and editable-line(values of keys)
  self.prompt, self.def_values = msg, def

  return msg, def
end

--
--
---@param input string
---@return boolean stop
---@return boolean? success (of command)
---@return string? message or ret from command
function C:check_builtin_commands(input)
  log_debug("check_builtin_commands dispatcher:%s", self.dispatcher ~= nil)
  if self.dispatcher then
    local _, args = U.get_line_n_command(input, ' ')
    local cmd = (args or E)[1]
    if cmd == M.CANCEL_ALIAS then
      return true, true, '\\q'
    end

    local dt = type(self.dispatcher)

    if cmd then
      log_debug('run builtin command: %s dispatcher:%s', args, dt)
      local code, called, ret = -1, false, ''

      if dt == 'table' then
        local handler = self.dispatcher[cmd]
        if type(handler) == 'function' then
          code, ret = handler(self.context, self.obj, args)
          called = true
        end
      elseif dt == 'function' then
        code, ret = self.dispatcher(self.context, self.obj, args)
        called = true
      end

      if not called then
        -- not found cmd or bad dispatcher
        local msg = U.format('cannot run cmd: %s dispatcher: %s',
          args, self.dispatcher)
        return true, false, msg
      else
        if code == nil and ret == nil then
          error('Seems the dispatcher is not returning any response')
        end
        return true, code == M.SUCCESS_EXIT_CODE, ret -- ok cmd performed
      end
    end
  end

  return false, nil, nil
end

--
-- parse user input to produce patch and run builtin commands
--
---@param prompt string - to sync with self.prompt
---@param input string
---@return boolean
---@return string|table|nil
---@return boolean stop
function C:parse_input(prompt, input)
  log_debug("parse_input", prompt, input)

  if not input or input == M.CANCEL_ALIAS or input == '' then
    return false, 'canceled', false
  end

  assert(type(prompt) == 'string', 'prompt')
  assert(type(input) == 'string', 'input')

  -- check built-in commands in the input
  local stop, success, ret = self:check_builtin_commands(input)
  if stop then
    log_debug('stop edit: performed command from input successed:%s', success)
    return success == true, ret, true
  end

  local ok, patch = self:parse_input_to_patch(prompt, input)
  self.patch = (type(patch) == 'table') and patch or nil

  log_debug("parse_input success:%s patch:%s", ok, patch)
  return ok, patch, false
end

---@param self self
---@param value_provider function
---@return table  patch
---@return number errors
local function build_patch(self, value_provider)
  local orderedkeys = assert_tbl_ne(self.orderedkeys, 'orderedkeys') -- order + types
  local snapshot = assert_tbl_ne(self.snapshot, 'snapshot')
  assert(type(value_provider) == 'function', 'value_provider')

  local patch = P:new({}, {
    rules = self.rules, -- bind one table for Editor and Patch
    data = self.serialize_opts
  })
  local errors = 0

  for i, e in ipairs(orderedkeys) do
    local key, declared_type = assert(e.k, 'key'), assert(e.t, 'type')
    if patch[key] then
      error(fmt("Attempt to override existed Key: '%s' at: %s", key, i))
    end
    ----
    local sync_key, new_value, nvs = value_provider(i, key, declared_type)
    assert(sync_key == key, 'enshure valid value_provider')

    local msg = '%s expected existed key:"%s" in snapshot: %s'
    local sn_wrapped_value = assert_tbl(snapshot[key], msg, i, key, snapshot)

    local old_original_value = sn_wrapped_value.o
    local nullable = sn_wrapped_value.nullable

    if nvs == M.TABLE_REF then
      new_value = old_original_value
    end

    if new_value == nil and old_original_value ~= nil and not nullable then
      P.report_set_error(patch, key, {
        id = M.TYPE_MISMATCH,
        details = {
          et = declared_type, -- expected type
          sr = nvs,           -- user input
          v = new_value,      -- parsed user input
        }
      })
      -- due to an input error, we assume that the value has not changed
      new_value = old_original_value
      errors = errors + 1
    end

    local patch_wrapped_value = {
      old = old_original_value,
      new = new_value,
    }
    ---
    if old_original_value == nil then
      patch_wrapped_value.old_nil = true
    end
    patch[key] = patch_wrapped_value
  end

  return patch, errors
end

--
-- with sync keys and args count
-- old name cli_parse
-- parse user input to patch
--
---@param line string
---@return boolean
---@return string|table
function C:parse_input_to_patch(prompt, line)
  assert_str(line, 'line')
  log_debug('parse_input_to_patch "%s" "%s"', prompt, line)

  local orderedkeys = assert_tbl_ne(self.orderedkeys, 'orderedkeys') -- order + types

  if line == self.def_values then
    return false, 'unchanged'
  end

  if prompt ~= self.prompt or not prompt then
    local msg = 'desynchronized, expected:"%s" got:"%s"'
    return false, fmt(msg, v2s(self.prompt), v2s(prompt))
  end

  local args = M.cli_parse_line(line)
  local ac, skc = #args, #orderedkeys

  if ac < skc then
    return false, fmt('expected at least: %s values got: %s', skc, ac)
  end
  if ac > skc then
    return false, fmt('too many values expected: %s got: %s', skc, ac)
  end

  local patch = build_patch(self, function(i, key, declared_type)
    --- args is an ordered list synchronized with orderedkeys
    local new_str_value = args[i] -- from user input, may be changed by user
    if new_str_value == M.TABLE_REF then
      return key, nil --[[use original value]], M.TABLE_REF
    end
    local new_value = C.str2value(new_str_value, declared_type, key)
    return key, new_value, new_str_value
  end)

  log_debug('parse_input parsed:', prompt, line, patch)
  self.patch = patch

  return true, patch
end

--
-- for patch entry
--  key = { -- wrapped_value
--    old = ..., new = ..., t = 'number', [old_nil = true]
--  }
---@param wrapped_value any -- table{[old|old_nil], new, t}
local function is_value_changed(wrapped_value, key)
  local wv = assert_tbl_ne(wrapped_value, 'wrapped_value', key)
  return not U.is_equal(wv.old, wv.new)
end

---@return string?
function C:build_readable_errors()
  if self then
    -- local sn = assert_tbl_ne(self.snapshot, 'snapshot')
    local patch = assert_tbl_ne(self.patch, 'patch')

    local t = patch:get_state()
    if t.errors then
      local s = ''
      for key, err in pairs(t.errors) do
        assert_tbl(err, key)
        if err.id == M.TYPE_MISMATCH and err.details then
          local exp_type = err.details.et -- expected type
          local new_value = err.details.v
          local input = v2s(err.details.sr)

          if s ~= '' then s = s .. "\n" end
          s = s .. fmt('key:\'%s\' expected value with type: %s, got: %s %s',
            v2s(key), v2s(exp_type), type(new_value), input)
        end
      end
      return s
    end
  end
  return nil
end

--
-- update all fields of the edited object based on the values received
-- from user input
--
---@return boolean has_changes
---@return table   report
function C:apply_patch()
  local patch = assert_tbl_ne(self.patch, 'patch') ---@cast patch clieos.Patch
  local obj = assert(self.obj, 'obj')

  local errors = P.errors_cnt(patch)
  if errors > 0 then
    ---@diagnostic disable-next-line: return-type-mismatch
    return false, self:build_readable_errors()
  end

  local nosync = false

  local method_name = self.names.apply_patch
  local update_fn = funOrDef(1, self.obj, method_name, nil)

  log_debug('apply_patch through object method:%s', update_fn ~= nil)

  if update_fn then
    -- apply whole patch in transaction way via object[mehtod] function
    if not is_function(update_fn) then
      error(fmt(
        'expected obj[%s] is function got: %s', method_name, type(update_fn)))
    end

    --[[local has_changes0, patch0 =]]
    update_fn(self.obj, patch)
    -- commit inside obj[method_name]() ?
  else
    -- simple direct editing of fields
    -- simple applying of the patch directly to the fields of the object

    -- todo: check is realy all given fields presend in object and
    -- has synchronized old values with patch and obj

    -- for _, e in pairs(self.orderedkeys) do
    --   local key = e.k
    --   patch:update_one_in(key, obj, nosync)
    -- end
    for key, wrapped_value in pairs(patch) do
      if is_value_changed(wrapped_value) then
        patch:update_one_in(key, obj, nosync)
      end
    end
  end

  local updated = P.commit(patch, obj, nosync)
  -- local changed = patch:changed_cnt()
  -- local updated = patch:updated_cnt()
  --
  -- -- to ensure all updated values was applyed
  -- assert_eq(changed, updated, 'changed_&_updated')

  return updated > 0, patch
end

-- static interface without direct access to clieos.Editor



--------------------------------------------------------------------------------
--                               Patch
--------------------------------------------------------------------------------
--
-- constructor
--
---@param o table?
---@return clieos.Patch
---@param opts table
function P:new(o, opts)
  assert(not o or type(o) == 'table', 'state can be nil or table')
  o = o or {}

  setmetatable(o, self)
  self.__index = self

  P.inject_state(o, opts)

  return o
end

function P.inject_state(patch, opts)
  opts = opts or E
  local inner_state = {
    updated = {}, -- to track touched keys and updated
    -- not_found
    -- assigned_nils
    -- blocked_nils
    -- rules -- to hold can_set_nil and so on def is nil
    rules = opts.rules,
    data = opts.serialize_data or opts.data,
  }
  -- inner_state.rules = opts.rules
  -- inner_state.data = opts.serialize_data or opts.data

  local middle = {
    get_state = function( --[[self]])
      return inner_state
    end,
    -- get_rules = function()
    --   return inner_state.rules
    -- end
  }
  setmetatable(middle, P)

  setmetatable(patch, middle)
  middle.__index = middle

  return patch
end

---@return table
function P:get_state()
  error('get_state method must be overridden in the constructor of clieos.Patch.'
    .. "\nSeems this table is not a clieos.Patch instance, use clieos.Patch:new()")
end

local function get_state(patch)
  assert_tbl(patch)
  if not is_function(patch.get_state) then
    P.inject_state(patch)
  end
  return patch:get_state()
end

---@return table?
function P:get_rules()
  if self then
    local t = get_state(self)
    t.rules = t.rules or {}
    return t.rules
  end
  return nil
end

function P:set_rule(key, value)
  if self then
    local t = get_state(self)
    t.rules = t.rules or {}
    t.rules[key] = value
  end
end

function P:get_rule(name, def)
  if self then
    assert(type(name) == 'string', 'name')
    local t = get_state(self)
    local v = (t.rules or E)[name]
    if v == nil then v = def end
    return v
  end
  return def
end

function P:get_data()
  if self then
    local t = get_state(self)
    return t.data
  end
  return nil
end

-- report

function P:report_set_nil_value(key)
  if self then
    assert(type(key) == 'string', 'key')
    local t = get_state(self)
    t.assigned_nils = t.assigned_nils or {}
    t.assigned_nils[key] = 1
  end
end

-- attempt to set null is blocked
function P:report_blocked_nil(key)
  if self and key then
    local t = get_state(self)
    t.blocked_nils = t.blocked_nils or {}
    t.blocked_nils[key] = 1
  end
end

--
-- mark given key as not found
--
-- track keys for which there were requests and which were not in the patch
function P:report_set_not_found(key)
  if self and key then
    local t = get_state(self)
    t.not_found = t.not_found or {}
    t.not_found[key] = 1 -- t.not_found or {}
    -- table.insert(t.not_found, key)
  end
end

---@param key string
---@param error table
function P:report_set_error(key, error)
  if self then
    assert(type(key) == 'string', 'key')
    assert(type(error) == 'table', 'error')
    local t = get_state(self)
    t.errors = t.errors or {}
    t.errors[key] = error
  end
end

--
-- check the synchronization of the specified values and the values
-- in the patch for reliability to eliminate errors.
-- used before update filed of objet (table key)
---@param key string
---@param old_value any
---@param new_value any
function P:validate_sync(key, old_value, new_value)
  if self then
    local msg = 'expected existed in patch wrapped_value for key: %s'
    local wrapped_value = assert_tbl(self[key], msg, key)
    assert_eq_in(wrapped_value.old, old_value, 'oldInPatch', 'givenOldVal', key)
    assert_eq_in(wrapped_value.new, new_value, 'oldInPatch', 'givenOldVal', key)
  end
end

-- for greater reliability, make sure that the value in the object has not
-- changed before the update
---@param name string
---@param inpatch any
---@param given any
---@param nosync boolean?
local function validate_value_sync(key, name, inpatch, given, nosync)
  if not nosync and given ~= nil then
    if inpatch ~= given then
      error(fmt(
        '%s: expected same %s values before update, got: inPatch:%s inObj:%s',
        v2s(key), v2s(name), v2s(inpatch), v2s(given)), 2)
    end
  end
end

--
-- to track changes when editing via the command line
-- mark the key as having a changed value
--
---@param self table?
---@param key string
---@param old_value any
---@param new_value any
function P:report_set_updated(key, old_value, new_value)
  if self and key then
    log_trace('updated key:"%s" old:%s new:%s', key, old_value, new_value, BT)

    P.validate_sync(self, key, old_value, new_value)

    local t = get_state(self)
    t.updated = t.updated or {}
    t.updated[key] = M.UPDATED_KEY
  end
end

-- mark as update without validate of synchronization old and new value
-- in the patch and in the object itself.
-- Idea: to provide the ability to manually update from within the object
-- itself so that you can mark those keys that were "taken into account" in this code
-- and not forgotten.
-- The idea is not to pass the code that did not apply all the updates from the
-- patch and to track such errors.
-- if there is an attempt to mark a key whose values have not changed in
-- the patch, that is, the old value is equal to the new one (after parsing the
-- user input), then simply ignore it and DO NOT mark as "updated" since in fact
-- there is nothing to update - nothing has changed.
function P:report_set_updated_risky(...)
  if self then
    assert(type(self) == 'table', 'expected table self(patch)')
    for i = 1, select('#', ...) do
      local key = select(i, ...)
      assert_str(key, '%s, expected a string key got: %s', i, key)

      local wrapped_value = rawget(self, key)
      if is_table(wrapped_value) then
        local prev_updated_st = nil
        local old_value, new_value = wrapped_value.old, wrapped_value.new

        -- mark as updated only if has chages.
        if not U.is_equal(old_value, new_value) then
          local t = get_state(self)
          t.updated = t.updated or {}
          prev_updated_st = t.updated[key]
          t.updated[key] = M.UPDATED_KEY
        end

        log_trace('updated key:"%s"(risky) old:%s new:%s (%s>%s)',
          key, old_value, new_value, prev_updated_st, M.UPDATED_KEY, BT)
      else
        log_trace('key:%s not exist in the patch', key)
      end
    end
  end
end

--[[
issue how to pass editor into user code? hold in patch:get_state?
function P:report_set_updated_autocheck(obj, editor)
  if is_table(self) then
    assert_tbl(obj, 'expected editable object is a table got: %s')
    local sn = editor:mk_snapshot()
    --
  end
  return false
end
]]

--
-- mark given key as requested by an external user code
--
-- this can be used to catch situations when an object is updated through
-- external code and this external code forgets to mark the requested new value
-- from the patch as updated in the object.
-- This is designed for cases where the editor cannot directly set values
-- in the object's fields, in cases where the object produces data from "phantom"
-- fields for serialization. Which, during deserialization, must also be
-- processed in a special way and not assigned directly into it table(as field).
---@param self table?
---@param key string
---@param old_value any
---@param new_value any
function P:report_set_touched(key, old_value, new_value)
  if self and key then
    log_trace('touched key:"%s" old:%s new:%s', key, old_value, new_value, BT)

    P.validate_sync(self, key, old_value, new_value)

    local t = get_state(self)
    t.updated = t.updated or {}
    t.updated[key] = M.TOUCHED_KEY -- touched but not mark as updated
  end
end

local function blocked_attempt_to_set_nil(patch, key, new_value, old_value)
  local can_set_nil = P.get_rule(patch, 'can_set_nil', false)

  if new_value == nil and old_value ~= nil then
    if not can_set_nil then
      P.report_blocked_nil(patch, key)
      return true
    end
    P.report_set_nil_value(patch, key)
  end

  return false
end


--
-- direct update.
--
-- used then given orderedkeys is a actual fileds of the editable object(table).
-- not suitable for cases when you need to process phantom fields.
--
-- throw an exception if the requested key is not in the patch table
--
-- sync old_value with patch before update if old_value provided
---@param patch table
---@param key string
---@param obj table
---@param nosync boolean?
function P.update_one_in(patch, key, obj, nosync)
  return P.update_one_in_sk(patch, key, obj, key, nosync)
end

--
-- direct update
-- using two keys with different names for the patch and object field that
-- correspond to each other.
--
-- used then given pkey(patch-key) is correspond to an actual filed of the
-- editable object (okey), but these names are different.
-- Not suitable for cases when you need to process phantom fields, because
-- it directly assigns a new value from the patch(by pkey) to the object field
-- by the given name(obj_name)
--
-- throw an exception if the requested key is not in the patch table
--
-- sync old_value with patch before update if old_value provided
--
---@param patch table
---@param pkey string    -- the name in patch (ordered key)
---@param obj table
---@param obj_key string -- the field name in object
---@param nosync boolean?
function P.update_one_in_sk(patch, pkey, obj, obj_key, nosync)
  assert_tbl(obj, 'editable_obj')
  assert_str(obj_key, 'object key')

  local msg = 'expected existed wrapped_value for key: %s, got: %s in patch: %s'
  local wrapped_value = assert_tbl(patch[pkey], msg, pkey, patch[pkey], patch)

  local old_value = obj[obj_key] -- or via getter?
  validate_value_sync(pkey, 'old', wrapped_value.old, old_value, nosync)

  local new_value = wrapped_value.new

  if blocked_attempt_to_set_nil(patch, pkey, new_value, old_value) then
    return false, old_value
  end

  if not U.is_equal(old_value, new_value) then -- and new_value ~= nil then
    -- TODO: use setter to update the value if present in obj  self.direct?
    obj[obj_key] = new_value
    -- mark as updated-in-object
    -- wrapped_value.u = wrapped_value.u or 1 -- ??
    P.report_set_updated(patch, pkey, old_value, new_value)
    return true, new_value
  end

  return false, old_value
end

--
-- update all keys in given patch
-- suitable only for direct update fields of object
-- not suitable for cases when you need to process phantom fields.
--
---@param patch table
---@param obj table
---@param nosync boolean?
function P.update_all(patch, obj, nosync)
  local updated = 0
  for key, wrapped_value in pairs(patch) do
    assert_tbl(wrapped_value, '%s: expected wrapped_value table, got:%s patch:%s',
      key, wrapped_value, patch)
    if not U.is_equal(wrapped_value.old, wrapped_value.new) then
      if P.update_one_in(patch, key, obj, nosync) then
        updated = updated + 1
      end
    end
  end

  updated = P.commit(patch, obj, nosync) or updated --?

  return updated > 0, patch
end

--
-- external update.
--
-- For cases when the object will update itself through the
-- METHOD_NAME_APPLY_PATCH method and using this helper function to access the
-- updated values from the patch
--
-- suitable for cases when you need to process phantom fields, when
-- some keys from given orderedkeys is not an actual fileds of the editable
-- object(table). And cannot be assigned directly
--
---@param patch table
---@param nosync boolean? risky without validations patch and obj values sync
---@return any, boolean
function P.update_one_out(patch, key, old_value, nosync)
  local msg = 'expected existed wrapped_value for key: %s, got: %s'
  local wrapped_value = assert_tbl(patch[key], msg, key)
  assert(old_value ~= nil, 'expected expility old_value, to pass nil use clieos.NIL')

  -- local old_value = obj[key]
  if old_value == M.NIL then old_value = nil end
  validate_value_sync(key, 'old', wrapped_value.old, old_value, nosync)

  local new_value = wrapped_value.new

  if blocked_attempt_to_set_nil(patch, key, new_value, old_value) then
    return old_value, false
  end

  if not U.is_equal(old_value, new_value) then -- and new_value ~= nil then
    -- obj[key] = new_value
    if P.get_rule(patch, 'trust', false) then
      -- mark as updated-in-object
      P.report_set_updated(patch, key, old_value, new_value)
    else
      P.report_set_touched(patch, key, old_value, new_value)
    end
    return new_value, true
  end

  return old_value, false
end

---@return number, boolean
function P.update_ex_n(patch, key, old_value, nosync)
  local res, updated = P.update_one_out(patch, key, old_value, nosync)
  if type(res) ~= 'number' then
    error(U.format("%s: expected number got:", key, res))
  end
  return res, updated
end

---@return string, boolean
function P.update_ex_s(patch, key, old_value, nosync)
  local res, updated = P.update_one_out(patch, key, old_value, nosync)
  if type(res) ~= 'string' then
    error(U.format("%s: expected string got:", key, res))
  end
  return res, updated
end

---@return boolean, boolean
function P.update_ex_b(patch, key, old_value, nosync)
  local res, updated = P.update_one_out(patch, key, old_value, nosync)
  if type(res) ~= 'boolean' then
    error(U.format("%s: expected boolean got:", key, res))
  end
  return res, updated
end

---@return table, boolean
function P.update_ex_t(patch, key, old_value, nosync)
  local res, updated = P.update_one_out(patch, key, old_value, nosync)
  if type(res) ~= 'table' then
    error(U.format("%s: expected table got:", key, res))
  end
  return res, updated
end

---@return any new_value
---@return boolean is_changed
---@return any old_value
function P.get_new_value(patch, key)
  local msg = 'expected existed wrapped_value for key: %s, got: %s'
  local wrapped_value = assert_tbl(patch[key], msg, key)
  local changed = not U.is_equal(wrapped_value.new, wrapped_value.old)
  return wrapped_value.new, changed, wrapped_value.old
end

function P.is_changed_to(patch, key, new_value)
  local new, changed, old = P.get_new_value(patch, key)
  return (changed and U.is_equal(new, new_value)), old, new
end

-- is the value of given key changed by user
---@param key string
function P:is_changed(key)
  if self then
    local wrapped_value = rawget(self, key)
    if wrapped_value then
      assert_tbl_ne(wrapped_value, '%s: expected wrapped_value got:%s', key)
      return is_value_changed(wrapped_value)
    end
    return false
  end
end

--
-- how many of the requested keys have changed values in this patch
--
---@return number
function P:changed_cnt_of_keys(...)
  local c = 0
  if self then
    for i = 1, select('#', ...) do
      local key = assert_str(select(i, ...), 'key')
      if P.is_changed(self, key) then c = c + 1 end
    end
  end
  return c
end

-- total number of changed object fields
---@return number
function P:changed_cnt()
  local c = 0
  if self then
    for key, wrapped_value in pairs(self) do
      assert_tbl_ne(wrapped_value, '%s: expected wrapped_value got:%s', key)
      if is_value_changed(wrapped_value, key) then c = c + 1 end
      log_debug('changed_cnt %s key:%s wv:%s', c, key, wrapped_value)
    end
  end
  return c
end

---@return boolean
function P:has_changes()
  if self then
    for key, wrapped_value in pairs(self) do
      assert_tbl_ne(wrapped_value, '%s: expected wrapped_value got:%s', key)
      if is_value_changed(wrapped_value, key) then
        return true
      end
    end
  end
  return false
end

---@param state number one of [M.TOUCHED_KEY M.UPDATED_KEY]
local function get_keys_updatedtype_cnt(patch, state)
  local c = 0
  if patch then
    local t = get_state(patch)
    for _, v in pairs(t.updated) do
      if v == state then c = c + 1 end
    end
  end
  return c
end

-- quantity of requests to user-modified fields of editable object
---@return number
function P:updated_cnt()
  return get_keys_updatedtype_cnt(self, M.UPDATED_KEY)
end

-- the quantity of external query which were not marked as updated in the object
-- the key is marked as touched when a new value is received in the
-- update_one_out method in order to check in the 'commit' method whether the
-- received value has been applied.
-- Designed for custom updating by the object itself based on data from the
-- given patch. via call METHOD_NAME_APPLY_PATCH
---@return number
function P:touched_cnt()
  return get_keys_updatedtype_cnt(self, M.TOUCHED_KEY)
end

-- keys whose values were updated in the patch but which were probably
-- forgotten and not applied to the object to update its state
function P:get_forgotten_keys()
  local t = {}
  if self then
    local st = get_state(self)
    local updated = st.updated
    for key, wrapped_value in pairs(self) do
      if is_value_changed(wrapped_value, key) then
        local updated_state = updated[key]
        if updated_state ~= M.UPDATED_KEY then
          t[key] = { w = wrapped_value, touched = updated_state == M.TOUCHED_KEY }
        end
      end
    end
  end

  return t
end

---@return number
function P:errors_cnt()
  local c = 0
  if self then
    local t = get_state(self)
    if t.errors then
      for _, _ in pairs(t.errors) do
        c = c + 1
      end
    end
  end
  return c
end

--
-- validate applyed patch
--
-- The goal is to check whether all updated keys have been applied to the
-- edited object. If not, cause an error and warn about which keys were not used
-- The main purpose is to provide the user of this module with a more reliable
-- way to perform custom updates directly from within their objects through
-- METHOD_NAME_APPLY_PATCH.
--
---@return number of updated files of editable object
---@diagnostic disable-next-line: unused-local
function P.commit(patch, obj, nosync)
  log_debug("commit", BT)
  local updated = P.updated_cnt(patch)
  -- TODO.call method obj.commit or obj.onUpdate

  if not nosync then
    local changed = P.changed_cnt(patch)

    -- to ensure all updated values was applyed
    if changed > updated then
      local forgottenkeys = P.get_forgotten_keys(patch)
      local msg = [[
WARNING keys changed:%s but mark as updated only:%s
after applying the patch state:%s
forgotten keys: %s

It looks like the patch has not been fully applied to your user code.
This message is intended to help catch bugs in the code. Perhaps you are using
an object state update from within the object itself and either did not apply
all the updated fields from the patch to the object itself or forgot to mark
them as updated. Use Patch.set_updated|set_updated_risky with P.update_one_out
]]
      error(U.format(msg, changed, updated, get_state(patch), forgottenkeys))
    end
  end

  return updated
end

--------------------------------------------------------------------------------
--                             testing helpers
--------------------------------------------------------------------------------

-- TODO rewrite
--
-- Usage example:
--   mk_snapshot0({'x', 123}, {'text', 'some-string'}, {''})
--                 ^key  ^value to obtain type
--
---@return table
function T.mk_snapshot0(...)
  local t = {}
  for i = 1, select('#', ...) do
    local e = select(i, ...)
    assert(type(e) == 'table', fmt('expected table got %s at i:%s', type(e), i))
    local k, v = e[1], e[2]
    assert(type(k) == 'string', 'expected key at [1]')
    assert(v ~= nil, 'expected value at [2]')
    assert(not e[3], 'expected pairs of key value got 3th element')

    local value, vtype = U.as_string(v)
    t[#t + 1] = { k = e[1], v = value, t = vtype }
  end
  return t
end

--
---@param modt table
function T.build_orderedkyes_from_modt(modt)
  local orderedkeys = {}
  local sorted = U.sort_keys(modt)
  for _, key in ipairs(sorted) do
    local orderedkey_entry = { k = key, t = type(modt[key]) }
    orderedkeys[#orderedkeys + 1] = orderedkey_entry
  end
  return orderedkeys
end

--
-- Convert short patch definition
--    {x = 3, y = 4, color = '@'}
--
-- into patch for M.METHOD_NAME_APPLY_PATCH:
--
--    local patch = {
--      color = { new = '@', old_nil = true },
--      x = { new = 3, old = 1 },
--      y = { new = 4, old = 2 }
--    }
--
-- To be able to create patches in one line (modt-oneliner)
-- For use in testing the behavior of the object editing method
--
---@param obj table
---@param modt table
---@param opts table?
function T.mk_patch(obj, modt, opts)
  assert(type(obj) == 'table', 'obj')
  local e = M.init(obj, opts) -- { data = serialize_opts }

  ---@diagnostic disable-next-line: unused-local
  local patch = build_patch(e, function(i, key, declared_type)
    local new_value = modt[key]
    -- assert(declared_type == type(new_value), 'synched') -- ?
    return key, new_value, v2s(new_value)
  end)

  return patch, e.orderedkeys
  -- if obj == nil
  --   -- auto build from modt
  --   local orderedkeys = T.build_orderedkyes_from_modt(modt)
  --   return T.mk_patch(modt, orderedkeys), orderedkeys
  -- end
end

return M
